#ifndef PID_H_
#define PID_H_

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include "common.h"

class PID : public TaskBase {
public:
	PID(char const *name, unsigned portBASE_TYPE priority, uint8_t number,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);
	~PID();

	void task();
	static void task_pid(void *param) {
		static_cast<PID *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	void PrintHandle();

	BaseType_t PidADCUpdate(float uV);

private:
	uint8_t m_uNumber;
	QueueHandle_t m_xQueueADCIn;
	TickType_t lastTick;

};

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* PID_H_ */
