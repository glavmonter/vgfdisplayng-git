#ifndef CANLIB_H_
#define CANLIB_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include "stm32f4xx_hal.h"
#include "common.h"
#include "controller.pb.h"
#include "pid.h"
#include "iadc.h"

class CANLib;

void CANLibLink(CANLib *ll);
void CANLibInit();

typedef enum _CANStatus{
	CS_WAIT_START = 0,
	CS_SEND_TO_HOST = 1,
	CS_RECEIVE_FROM_HOST = 2
} CANStatus;


/* Проверка на максимально возможный размер пакета */
#if (VGF_Protocol_size > 1792)	// Больше, чем 1792 байта не передать
#error "Protocol buffer too big. Please make in smaller!!!!"
#endif

/*
 * Передача данных:
 * Первый пакет CANMSG_DTH или CANMSG_HTD
 * 2 байта - размер передаваемого сообщения в байтах (65535 максимум)
 * 4 байта - CRC32
 *
 * Последующие пакеты CANMSG_DATA. Передаем по 7 байт за пакет
 * 1 байт - номер пакета (максимум 255 пакетов по 7 байт = 1792 байта)
 * остальные байты - данные.
 *
 * Ожидаем CAMNSG_ACK или NACK за 100 мс.
 */
class CANLib : public TaskBase {
public:
	CANLib(char const *name, unsigned portBASE_TYPE priority, uint8_t canID,
			uint16_t stackDepth);
	~CANLib();

	void task();
	static void task_can(void *param) {
		static_cast<CANLib *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	QueueHandle_t CANRxQueue;

	void SetPids(PID **p);
	void SetIAdc(IADC *l) { iadc = l; }

	void Ping();

private:
	IADC *iadc;

	uint8_t DeviceID;
	CANStatus status;

	CAN_HandleTypeDef hcan;

	int32_t SendToHost();
	int32_t ReceiveFromHost(CanRxMsgTypeDef *RxMessage);

	void SendConfigToHost();
	void ProcessColdJunction();

	int32_t ProccessInputPb(VGF_Protocol *pb);

	void SendACK();
	void SendNACK();
	SemaphoreHandle_t xMutex;

	/* ПИДы для чтения и передачи данных */
	PID **m_pPids;


};

#define CAN_OK 				(0)
#define CAN_ERROR_TIMEOUT	(-1)
#define CAN_ERROR_ACK		(-2)
#define CAN_ERROR_MEMORY	(-3)
#define CAN_ERROR_SEMPTH	(-4)
#define CAN_ERROR_PB		(-5)
#define CAN_ERROR_UNEXP		(-6)

#ifdef __cplusplus
extern "C" {
#endif

void CAN1_RX0_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif /* CANLIB_H_ */
