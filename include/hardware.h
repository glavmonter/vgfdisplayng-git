/*
 *  @file VGFDisplayNg/hardware.h
 *  @date 16.10.2014
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Описание выводов, и портов
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_
#include "stm32f4xx_hal.h"


#define LED0_ON()							GPIOE->BSRRH = GPIO_PIN_13
#define LED0_OFF()							GPIOE->BSRRL = GPIO_PIN_13
#define LED0_TOGGLE()						GPIOE->ODR ^= GPIO_PIN_13

#define LED1_ON()							GPIOE->BSRRH = GPIO_PIN_12
#define LED1_OFF()							GPIOE->BSRRL = GPIO_PIN_12
#define LED1_TOGGLE()						GPIOE->ODR ^= GPIO_PIN_12

#define LED2_ON()							GPIOE->BSRRH = GPIO_PIN_11
#define LED2_OFF()							GPIOE->BSRRL = GPIO_PIN_11
#define LED2_TOGGLE()						GPIOE->ODR ^= GPIO_PIN_11


#define BUZZ_ON()							GPIOD->BSRRL = GPIO_PIN_1
#define BUZZ_OFF()							GPIOD->BSRRH = GPIO_PIN_1
#define BUZZ_TOGGLE()						GPIOD->ODR ^= GPIO_PIN_1

#endif /* HARDWARE_H_ */
