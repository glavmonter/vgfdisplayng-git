/*
 *  @file VGFThermo/lmp90100.h
 *  @date 24.12.2012
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 */

#ifndef LMP90100_H_
#define LMP90100_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include "SEGGER_RTT.h"
#include "common.h"
#include "pid.h"

#ifdef __cplusplus
 extern "C" {
#endif

/* Register map */
#define LMP90100_RESETCN_REG						(0x00)             /* Reset Control */

#define LMP90100_SPI_HANDSHAKECN_REG				(0x01)             /* SPI Handshake Control */
#define LMP90100_SPI_RESET_REG						(0x02)             /* SPI Reset Control */
#define LMP90100_SPI_STREAMCN_REG					(0x03)             /* SPI Stream Control*/

#define LMP90100_PWRCN_REG							(0x08)             /* Power Mode Control and Status */
#define LMP90100_DATA_ONLY_1_REG					(0x09)             /* Data Only Read Control 1 */
#define LMP90100_DATA_ONLY_2_REG					(0x0A)             /* Data Only Read Control 2 */
#define LMP90100_ADC_RESTART_REG					(0x0B)             /* ADC Restart Conversion */

#define LMP90100_GPIO_DIRCN_REG						(0x0E)             /* GPIO Direction Control */
#define LMP90100_GPIO_DAT_REG						(0x0F)             /* GPIO Data */

#define LMP90100_BGCALCN_REG						(0x10)             /* Background Calibration Control */

#define LMP90100_SPI_DRDYBCN_REG					(0x11)             /* SPI Data Ready Bar Control */


#define LMP90100_ADC_AUXCN_REG						(0x12)             /* ADC Auxillary Control */
#define LMP90100_SPI_CRC_CN_REG						(0x13)             /* CRC Control */
#define LMP90100_SENDIAG_THLDH_REG					(0x14)             /* Sensor Diagnostic Threshold High */
#define LMP90100_SENDIAG_THLDL_REG					(0x15)             /* Sensor Diagnostic Threshold Low */

#define LMP90100_SCALCN_REG							(0x17)             /* System Calibration Control */
#define LMP90100_ADC_DONE_REG						(0x18)             /* ADC Data Available */
#define LMP90100_SENDIAG_FLAGS_REG					(0x19)             /* Sensor Diagnostic Flags */
#define LMP90100_ADC_DOUT2_REG						(0x1A)             /* ADC Conversion Data 2 */
#define LMP90100_ADC_DOUT1_REG						(0x1B)             /* ADC Conversion Data 1 */
#define LMP90100_ADC_DOUT0_REG						(0x1C)             /* ADC Conversion Data 0 */
#define LMP90100_SPI_CRC_DAT_REG					(0x1D)             /* CRC Data */

#define LMP90100_CH_STS_REG							(0x1E)             /* Channel Status */
#define LMP90100_CH_SCAN_NRDY						(0x02)             /* Channel Scan Not Ready Bit */

#define LMP90100_CH_SCAN_REG						(0x1F)             /* Channel Scan Mode */
#define LMP90100_CH0_INPUTCN_REG					(0x20)             /* CH0 Input Control */
#define LMP90100_CH0_CONFIG_REG						(0x21)             /* CH0 Configuration */
#define LMP90100_CH1_INPUTCN_REG					(0x22)             /* CH1 Input Control */
#define LMP90100_CH1_CONFIG_REG						(0x23)             /* CH1 Configuration */
#define LMP90100_CH2_INPUTCN_REG					(0x24)             /* CH2 Input Control */
#define LMP90100_CH2_CONFIG_REG						(0x25)             /* CH2 Configuration */
#define LMP90100_CH3_INPUTCN_REG					(0x26)             /* CH3 Input Control */
#define LMP90100_CH3_CONFIG_REG						(0x27)             /* CH3 Configuration */
#define LMP90100_CH4_INPUTCN_REG					(0x28)             /* CH4 Input Control */
#define LMP90100_CH4_CONFIG_REG						(0x29)             /* CH4 Configuration */
#define LMP90100_CH5_INPUTCN_REG					(0x2A)             /* CH5 Input Control */
#define LMP90100_CH5_CONFIG_REG						(0x2B)             /* CH5 Configuration */
#define LMP90100_CH6_INPUTCN_REG					(0x2C)             /* CH6 Input Control */
#define LMP90100_CH6_CONFIG_REG						(0x2D)             /* CH6 Configuration */

#define LMP90100_CH0_SCAL_OFFSET2_REG				(0x30)             /* CH0 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH0_SCAL_OFFSET1_REG				(0x31)             /* CH0 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH0_SCAL_OFFSET0_REG				(0x32)             /* CH0 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH0_SCAL_GAIN2_REG					(0x33)             /* CH0 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH0_SCAL_GAIN1_REG					(0x34)             /* CH0 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH0_SCAL_GAIN0_REG					(0x35)             /* CH0 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH0_SCAL_SCALING_REG				(0x36)             /* CH0 System Calibration Scaling Coefficient */
#define LMP90100_CH0_SCAL_BITS_SELECTOR_REG			(0x37)             /* CH0 System Calibration Bits Selector */

#define LMP90100_CH1_SCAL_OFFSET2_REG				(0x38)             /* CH1 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH1_SCAL_OFFSET1_REG				(0x39)             /* CH1 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH1_SCAL_OFFSET0_REG				(0x3A)             /* CH1 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH1_SCAL_GAIN2_REG					(0x3B)             /* CH1 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH1_SCAL_GAIN1_REG					(0x3C)             /* CH1 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH1_SCAL_GAIN0_REG					(0x3D)             /* CH1 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH1_SCAL_SCALING_REG				(0x3E)             /* CH1 System Calibration Scaling Coefficient */
#define LMP90100_CH1_SCAL_BITS_SELECTOR_REG			(0x3F)             /* CH1 System Calibration Bits Selector */

#define LMP90100_CH2_SCAL_OFFSET2_REG				(0x40)             /* CH2 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH2_SCAL_OFFSET1_REG				(0x41)             /* CH2 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH2_SCAL_OFFSET0_REG				(0x42)             /* CH2 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH2_SCAL_GAIN2_REG					(0x43)             /* CH2 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH2_SCAL_GAIN1_REG					(0x44)             /* CH2 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH2_SCAL_GAIN0_REG					(0x45)             /* CH2 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH2_SCAL_SCALING_REG				(0x46)             /* CH2 System Calibration Scaling Coefficient */
#define LMP90100_CH2_SCAL_BITS_SELECTOR_REG			(0x47)             /* CH2 System Calibration Bits Selector */

#define LMP90100_CH3_SCAL_OFFSET2_REG				(0x48)             /* CH3 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH3_SCAL_OFFSET1_REG				(0x49)             /* CH3 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH3_SCAL_OFFSET0_REG				(0x4A)             /* CH3 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH3_SCAL_GAIN2_REG					(0x4B)             /* CH3 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH3_SCAL_GAIN1_REG					(0x4C)             /* CH3 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH3_SCAL_GAIN0_REG					(0x4D)             /* CH3 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH3_SCAL_SCALING_REG				(0x4E)             /* CH3 System Calibration Scaling Coefficient */
#define LMP90100_CH3_SCAL_BITS_SELECTOR_REG			(0x4F)             /* CH3 System Calibration Bits Selector */

// Useful definitions
#define LMP90100_URA_END							(0xFF)
#define LMP90100_URA_MASK							(0x70)
#define LMP90100_LRA_MASK							(0x0F)
#define LMP90100_READ_BIT							(0x80)
#define LMP90100_WRITE_BIT							(0x00)
#define LMP90100_SIZE_1B							(0x00)
#define LMP90100_SIZE_2B							(0x20)
#define LMP90100_SIZE_3B							(0x40)
#define LMP90100_SIZE_STREAM						(0x60)

#define LMP90100_CH_NUM_MASK						(0x07)
#define LMP90100_SENDIAG_SHORT						(0x80)
#define LMP90100_SENDIAG_RAILS						(0x40)
#define LMP90100_SENDIAG_POR						(0x20)
#define LMP90100_SENDIAG_OFLO						(0x18)

#define LMP90100_INSTRUCTION_BYTE1_WRITE			(0x10)
#define LMP90100_DATA_FIRST_MODE_INSTRUCTION_ENABLE	(0xFA)
#define LMP90100_DATA_FIRST_MODE_INSTRUCTION_DISABLE	(0xFB)
#define LMP90100_DATA_FIRST_MODE_INSTRUCTION_READ_MODE_STATUS	(0x9F)
#define LMP90100_DATA_FIRST_MODE_STATUS_FLAG		(0x80)

#define LMP90100_GAIN_1								(0x0 << 1)
#define LMP90100_GAIN_2								(0x1 << 1)
#define LMP90100_GAIN_4								(0x2 << 1)
#define LMP90100_GAIN_8								(0x3 << 1)
#define LMP90100_GAIN_16							(0x4 << 1)
#define LMP90100_GAIN_32							(0x5 << 1)
#define LMP90100_GAIN_64							(0x6 << 1)
#define LMP90100_GAIN_128							(0x7 << 1)

enum crc_check { CRC_PASS, CRC_FAIL, CRC_NOT_USED};

// CRC-8: x8 + x5 + x4 + 1
#define CRC8_POLY          0x31
#define CRC8_INIT_REM      0x0
#define CRC8_FINAL_XOR     0xFF


/*
 * Время переключения до обработки данных по семафору
 * _______               _______
 * DRDY   |_____________|
 *        |
 * _______|__              _____
 * NSS    |  |____________|
 *        |  |
 *        18us
 */
class LMP90100;
class FilterKalman;

void LMPInit();
void LMPLink(LMP90100 *ll);
void EXTI4_IRQHandler(void);

#ifdef __cplusplus
}
#endif

typedef enum _STATE_PRESSURE {
	PRESSURE_NOT_CALIBRATED,
	PRESSURE_CALIBRATED,

	PRESSURE_START_CALIBRATING,
	PRESSURE_PROCESS_CALIBRATING,
} STATE_PRESSURE;

typedef enum _STATE_CALIBRATION {
	CALIB_STABILIZATION,
	CALIB_CALIBRATION
} STATE_CALIBRATION;

#define CALIBRATING_START	0x01
#define CALIBRATING_STOP	0x02

#define STABILIZATION_TIME	30000		/* Время стабилизации давлени в тиках */
#define CALIBRATION_TIME	30000		/* Время калибровки давления в тиках */


class LMP90100 : public TaskBase {
public:
	LMP90100(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);
	~LMP90100();

	void task();
	static void task_adc(void *param) {
		static_cast<LMP90100 *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	SemaphoreHandle_t xSemaphoreADC;

	bool GetPressure_kPa(double *Pressure);		/* Получить значение давления в кПа */

	void StartCalibration();					/* Запустить процесс калибровки */
	void StopCalibration();						/* Принудительно остановить калибровку */
	STATE_PRESSURE GetCalibratedStatus();		/* Запросить статус калибровки */
	bool SetCalibrationValue(double err);		/* Установить значение калибровки (если известно) */
	bool GetCalibrationValue(double *err);		/* Получить значение текущей калибровки */

	bool GetAdcData(double *ch0, double *ch1);	/* Получить данные АЦП по первым двум каналам */

private:
	uint8_t m_uURA;

	SPI_HandleTypeDef hspi;
	TIM_HandleTypeDef htim;

	inline void NSS_LOW() {GPIOA->BSRRH = GPIO_PIN_4; }  // PA4
	inline void NSS_HIGH() {GPIOA->BSRRL = GPIO_PIN_4; } // PA4

	void WriteReg(uint8_t addr, uint8_t value);
	uint8_t ReadReg(uint8_t addr);
	void WriteRegSettings();
	void EnableDataFirstMode(uint8_t addr, uint8_t count);

	uint8_t SPICRCCheck (uint8_t *buffer, uint8_t count);
	uint8_t crc8MakeBitwise2(uint8_t crc, uint8_t poly, uint8_t *pmsg, uint8_t msg_size);

	SemaphoreHandle_t xMutex;
	QueueHandle_t xQueueCalibrate;
	QueueSetHandle_t xQueueSet;

	STATE_PRESSURE ProcessCalibrate(double Pressure);

	void ProcessPressure(uint8_t ch_num, uint8_t sendiag, int32_t ch_data);

	/* Считанные данные термопар в мкВ */
	double Tc_uV[2];
	uint8_t Tc_diag[2];



	/* Считанные данные напряжения питания датчика давления */
	double Vs_uV;
	double Vs_V;
	uint8_t Vs_diag;
	bool Vs_Valid;

	/* Считанные данные датчика давления в мкВ */
	double Error;
	STATE_PRESSURE StatePressure;
	double Vo_uV;
	bool Vo_Valid;

	double Pressure_kPa;
	uint8_t Vo_diag;

	FilterKalman *filterPressure;
};


class FilterKalman {
public:
	FilterKalman(double q, double r, double xEstLast, double PLast, double xEst);
	~FilterKalman();

	double Q;
	double R;
	double Process(double data_in);

private:
	double x_est_last;
	double P_last;
	double x_est;
};

#endif /* LMP90100_H_ */
