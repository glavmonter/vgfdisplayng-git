#ifndef MAIN_H_
#define MAIN_H_


#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include "common.h"
#include "SEGGER_RTT.h"

#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"


#ifdef __cplusplus
extern "C" {
#endif

void TIM7_IRQHandler();

void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_H_ */
