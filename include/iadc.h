#ifndef IADC_H_
#define IADC_H_

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include "common.h"
#include "stm32f4xx_hal.h"

class IADC;
void IADCInit();
void IADCLink(IADC *ll);


#define NUM_TEMPERATURES	80
#define NUM_TELEMETRY		4000

typedef enum _BANK {
	BANK_1,
	BANK_2
} BANK;

class IADC : public TaskBase {
public:
	IADC(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);
	~IADC();

	void task();
	static void task_iadc(void *param) {
		static_cast<IADC *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

public:
	float GetCPUTemperature();
	float GetPowerUmV();
	float GetPowerImA();
	void GetPowerUI(float *UmV, float *ImA);

	SemaphoreHandle_t xSemaphoreISRADC1;
	SemaphoreHandle_t xSemaphoreISRADC2;

	ADC_HandleTypeDef hadc1;
	ADC_HandleTypeDef hadc2;

private:
	uint16_t *datas_temp;
	uint16_t *datas_tele_bank_1;
	uint16_t *datas_tele_bank_2;

	/* Результирующие значения температуры и параметров тока и напряжения */
	float PowerU;
	float PowerI;
	float TemperatureCPU;

	BANK current_bank;

	TIM_HandleTypeDef htimADC1;
	TIM_HandleTypeDef htimADC2;

	QueueSetHandle_t xQueueSet;

	uint32_t CalcRMS(uint16_t *datas, uint32_t size, float *Urms, float *Irms);

	SemaphoreHandle_t xMutexTemperature;
	SemaphoreHandle_t xMutexPowerData;
};

#ifdef __cplusplus
extern "C" {
#endif

void DMA2_Stream0_IRQHandler();
void DMA2_Stream2_IRQHandler();
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle);

#ifdef __cplusplus
}
#endif

#endif /* PID_H_ */
