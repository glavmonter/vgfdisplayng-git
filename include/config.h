/*
 *  @file VGFThermoNg/config.h
 *  @date 06.01.2013
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Конфигурация АЦП
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <FreeRTOS.h>
#include <task.h>

/*
 * Конфигурационный дефайны: приоритеты тасков, включение отладочной информации и прочее
 *
 */

/* configMAX_PRIORITIES - 5 */
#define configPRIORITY_ADC		2
#define configPRIORITY_CAN		3
#define configPRIORITY_PID		4
#define configPRIORITY_STATS	tskIDLE_PRIORITY	// 0

#define LMP90100_SPEED								(0x10) 			   /* Скорость выборки (3.3 SPS) */

#define LMP90100_RESETCN_REG_VALUE                  (0xC3)             /* Register and Conversion Reset */
#define LMP90100_SPI_HANDSHAKECN_REG_VALUE          (0x01)             /* SPI SDO High Z Delayed */

#define LMP90100_SPI_STREAMCN_REG_VALUE             (0x00)             /* SPI Normal Streaming mode */
#define LMP90100_PWRCN_REG_VALUE                    (0x00)             /* Active Mode */

#define LMP90100_ADC_RESTART_REG_VALUE              (0x00)             /* Disable restart Conversion */

#define LMP90100_GPIO_DIRCN_REG_VALUE               (0x40)             /* D6 output, D0-D5 inputs */
#define LMP90100_GPIO_DAT_REG_VALUE                 (0x40)             /* Set D6 high, others ignore */

#define LMP90100_BGCALCN_REG_VALUE                  (0x02)             /* Background Calibration OFF */

#define LMP90100_SPI_DRDYBCN_REG_VALUE              (0x83)             /* Enable DRDYB on D6, bits 0 & 1 must be 1, others default */

#define LMP90100_ADC_AUXCN_REG_VALUE                (0x30)             /* bypass external clock detection, external clock, select 0uA RTD current */
#define LMP90100_SPI_CRC_CN_REG_VALUE               (0x14)             /* enable CRC, Bit 3 must be 0, DRDYB is deasserted after CRC is read */
#define LMP90100_SENDIAG_THLDH_REG_VALUE            (0x00)             /* Sensor Diagnostic Threshold High */
#define LMP90100_SENDIAG_THLDL_REG_VALUE            (0x00)             /* Sensor Diagnostic Threshold Low */

#define LMP90100_SCALCN_REG_VALUE                   (0x00)             /* System Calibration Control Normal Mode */
#define LMP90100_ADC_DONE_REG_VALUE                 (0xFF)             /* ADC Data unAvailable */
#define LMP90100_SENDIAG_FLAGS_REG_VALUE            (0x00)             /* Sensor Diagnostic Flags - status output */
#define LMP90100_ADC_DOUT2_REG_VALUE                (0x00)             /* ADC Conversion Data 2 */
#define LMP90100_ADC_DOUT1_REG_VALUE                (0x00)             /* ADC Conversion Data 1 */
#define LMP90100_ADC_DOUT0_REG_VALUE                (0x00)             /* ADC Conversion Data 0 */
#define LMP90100_SPI_CRC_DAT_REG_VALUE              (0xFF)             /* Reset CRC Data */

#define LMP90100_CH_STS_REG_VALUE                   (0x00)             /* Channel Status */
#define LMP90100_CH_SCAN_REG_VALUE                  (0xD8)             /* Multiple Channel Continuous Scan with burnout: Channel 0, 1, 2, 3 */

#define LMP90100_CH0_INPUTCN_REG_VALUE              (0x81)             /* enable sensor diagnostics, default ref, vinp 0 vinn 1 */
#define LMP90100_CH0_CONFIG_REG_VALUE               (0x1C)             /* CH0 Configuration: 3.355SPS, FGA 64, buffer in signal path*/

#define LMP90100_CH1_INPUTCN_REG_VALUE              (0x93)             /* enable sensor diagnostics, default ref, vinp 2 vinn 3 */
#define LMP90100_CH1_CONFIG_REG_VALUE               (0x1C)             /* CH1 configuration: 3.355SPS, FGA 64, buffer in signal path */

#define LMP90100_CH2_INPUTCN_REG_VALUE              (0x25)             /* disable sensor diagnostics, default ref, vinp 4 vinn 5 */
#define LMP90100_CH2_CONFIG_REG_VALUE               (0x12)             /* CH2 configuration: 3.355SPS, FGA 2, buffer in signal path */

#define LMP90100_CH3_INPUTCN_REG_VALUE              (0x37)             /* disable sensor diagnostics, default ref, vinp 6 vinn 7 */
#define LMP90100_CH3_CONFIG_REG_VALUE               (0x10)             /* CH3 configuration: 3.55SPS, FGA 1, buffer in signal path */

#define LMP90100_CH4_INPUTCN_REG_VALUE              (0x81)             /* disable sensor diagnostics, default ref, vinp 2 vinn 3 */
#define LMP90100_CH4_CONFIG_REG_VALUE               (0x70)             /* 214.65SPS, FGA off, buffer in signal path */

#define LMP90100_CH5_INPUTCN_REG_VALUE              (0x93)             /* disable sensor diagnostics, default ref, vinp 4 vinn 5 */
#define LMP90100_CH5_CONFIG_REG_VALUE               (0x70)             /* 214.65SPS, FGA off, buffer in signal path */

#define LMP90100_CH6_INPUTCN_REG_VALUE              (0xA5)             /* disable sensor diagnostics, default ref, vinp 6 vinn 7 */
#define LMP90100_CH6_CONFIG_REG_VALUE               (0x70)             /* 214.65SPS, FGA off, buffer in signal path */

#define LMP90100_CH0_SCAL_OFFSET2_REG_VALUE         (0x00)             /* CH0 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH0_SCAL_OFFSET1_REG_VALUE         (0x00)             /* CH0 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH0_SCAL_OFFSET0_REG_VALUE         (0x00)             /* CH0 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH0_SCAL_GAIN2_REG_VALUE           (0x80)             /* CH0 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH0_SCAL_GAIN1_REG_VALUE           (0x00)             /* CH0 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH0_SCAL_GAIN0_REG_VALUE           (0x00)             /* CH0 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH0_SCAL_SCALING_REG_VALUE         (0x00)             /* CH0 System Calibration Scaling Coefficient */
#define LMP90100_CH0_SCAL_BITS_SELECTOR_REG_VALUE   (0x00)             /* CH0 System Calibration Bits Selector */

#define LMP90100_CH1_SCAL_OFFSET2_REG_VALUE         (0x00)             /* CH1 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH1_SCAL_OFFSET1_REG_VALUE         (0x00)             /* CH1 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH1_SCAL_OFFSET0_REG_VALUE         (0x00)             /* CH1 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH1_SCAL_GAIN2_REG_VALUE           (0x80)             /* CH1 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH1_SCAL_GAIN1_REG_VALUE           (0x00)             /* CH1 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH1_SCAL_GAIN0_REG_VALUE           (0x00)             /* CH1 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH1_SCAL_SCALING_REG_VALUE         (0x00)             /* CH1 System Calibration Scaling Coefficient */
#define LMP90100_CH1_SCAL_BITS_SELECTOR_REG_VALUE   (0x00)             /* CH1 System Calibration Bits Selector */

#define LMP90100_CH2_SCAL_OFFSET2_REG_VALUE         (0x00)             /* CH2 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH2_SCAL_OFFSET1_REG_VALUE         (0x00)             /* CH2 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH2_SCAL_OFFSET0_REG_VALUE         (0x00)             /* CH2 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH2_SCAL_GAIN2_REG_VALUE           (0x80)             /* CH2 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH2_SCAL_GAIN1_REG_VALUE           (0x00)             /* CH2 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH2_SCAL_GAIN0_REG_VALUE           (0x00)             /* CH2 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH2_SCAL_SCALING_REG_VALUE         (0x00)             /* CH2 System Calibration Scaling Coefficient */
#define LMP90100_CH2_SCAL_BITS_SELECTOR_REG_VALUE   (0x00)             /* CH2 System Calibration Bits Selector */

#define LMP90100_CH3_SCAL_OFFSET2_REG_VALUE         (0x00)             /* CH3 System Calibration Offset Coefficient [23:16] */
#define LMP90100_CH3_SCAL_OFFSET1_REG_VALUE         (0x00)             /* CH3 System Calibration Offset Coefficient [15:8] */
#define LMP90100_CH3_SCAL_OFFSET0_REG_VALUE         (0x00)             /* CH3 System Calibration Offset Coefficient [7:0] */
#define LMP90100_CH3_SCAL_GAIN2_REG_VALUE           (0x80)             /* CH3 System Calibration Gain Coefficient [23:16]  */
#define LMP90100_CH3_SCAL_GAIN1_REG_VALUE           (0x00)             /* CH3 System Calibration Gain Coefficient [15:8]  */
#define LMP90100_CH3_SCAL_GAIN0_REG_VALUE           (0x00)             /* CH3 System Calibration Gain Coefficient [7:0]  */
#define LMP90100_CH3_SCAL_SCALING_REG_VALUE         (0x00)             /* CH3 System Calibration Scaling Coefficient */
#define LMP90100_CH3_SCAL_BITS_SELECTOR_REG_VALUE   (0x00)             /* CH3 System Calibration Bits Selector */


#endif /* CONFIG_H_ */
