#ifndef MAX6954_H_
#define MAX6954_H_

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include "common.h"
#include "stm32f4xx_hal.h"

class MAX6954;
void MAX6954Link(MAX6954 *ll);
void MAX6954Init();

typedef enum _BRIGHTNESS {
	BRIGHTNESS_MIN = 0x0,
	BRIGHTNESS_1_16 = 0x0,
	BRIGHTNESS_2_16 = 0x1,
	BRIGHTNESS_3_16 = 0x2,
	BRIGHTNESS_4_16 = 0x3,
	BRIGHTNESS_5_16 = 0x4,
	BRIGHTNESS_6_16 = 0x5,
	BRIGHTNESS_7_16 = 0x6,
	BRIGHTNESS_8_16 = 0x7,
	BRIGHTNESS_9_16 = 0x8,
	BRIGHTNESS_10_16 = 0x9,
	BRIGHTNESS_11_16 = 0xA,
	BRIGHTNESS_12_16 = 0xB,
	BRIGHTNESS_13_16 = 0xC,
	BRIGHTNESS_14_16 = 0xD,
	BRIGHTNESS_15_16 = 0xE,
	BRIGHTNESS_MAX	= 0xF
} BRIGHTNESS;


typedef enum _LINE {
	LINE_0 = 0,
	LINE_1 = 1,
	LINE_2 = 2,
	LINE_3 = 3
} LINE;

typedef enum _ButtonState {
	BTN_PRESSED,
	BTN_RELEASED
} ButtonState;

typedef struct _ButtonEvent {
	uint8_t Button;		/* Номер кнопки */
	ButtonState State;	/* Ее состояние: нажатие, отпускание */
	TickType_t Tick;	/* Время, в которое произошло событие */
} ButtonEvent;


class MAX6954 : public TaskBase {
public:
	MAX6954(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth = configMINIMAL_STACK_SIZE);
	~MAX6954();

	void task();
	static void task_max(void *param) {
		static_cast<MAX6954 *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}


public:
	/* Потокобезопасно */
	void PrintLine(LINE line, int32_t value);
	void PrintLine(LINE line, float value);
	void PrintLine(LINE line, float value, uint8_t precission);

	void PrintCustom(LINE line, uint8_t seg1, uint8_t seg2, uint8_t seg3, uint8_t seg4);
	void PrintCustom(LINE line, uint32_t segments);

	SemaphoreHandle_t 	xSemaphoreISR;
	QueueHandle_t		xQueueButtonEvent;

private:
	inline void NSS_LOW() {GPIOB->BSRRH = GPIO_PIN_12;}
	inline void NSS_HIGH() {GPIOB->BSRRL = GPIO_PIN_12;}

	SPI_HandleTypeDef hspi;
	SemaphoreHandle_t xMutex;

	void SendData(uint8_t address, uint8_t data);
	uint8_t ReadReg(uint8_t address);

	void EnableDecodeMode();
	void DisableDecodeMode();

	void EnableGlobalBrightness();
	void DisableGlobalBrightness();
	void SetGlobalBrightness(BRIGHTNESS brightness);
	void ClearAll();

	void SelfTest();

	char *floatToString(char *outstr, double value, int places, int minwidth = 0, bool rightjustify = false);

	uint8_t DecodeInt(uint8_t data, uint8_t dp);

	uint8_t characters[16];
	const uint8_t letters[16] = {0x7E, 0x30, 0x6d, 0x79, 0x33, 0x5b, 0x5F, 0x70, 0x7F, 0x7B, 0x77, 0x1F, 0x4E, 0x3D, 0x4F, 0x47};

	const uint8_t lines[4][4] = {	{0x20, 0x28, 0x22, 0x2A},
							{0x21, 0x29, 0x23, 0x2B},
							{0x24, 0x2C, 0x26, 0x2E},
							{0x25, 0x2D, 0x27, 0x2F}};

};

#ifdef __cplusplus
extern "C" {
#endif

void EXTI3_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif /* MAX6954_H_ */
