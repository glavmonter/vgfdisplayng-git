/*
 * stepper.h
 *
 *  Created on: 16 oct 2014
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */

#ifndef STEPPER_H_
#define STEPPER_H_

#include <FreeRTOS.h>
#include <timers.h>
#include <stack>
#include <list>
#include "stm32f4xx_hal.h"
#include "common.h"
#include "step_config.h"

void StepperStart();

void GetProgramm(int id, StepConfig *s);

typedef enum _DIRECTION {
	DIR_UP,
	DIR_DOWN
} DIRECTION;

typedef struct _S_PROGRAM {
	double Speed_mm_h;
	uint32_t Time;
} StepperProgram_t;

class Stepper : public TaskBase {
public:
	Stepper(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth);
	~Stepper();

	void task();

	static void task_stepper(void *param) {
		static_cast<Stepper *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	static void TimerCall(TimerHandle_t pxTimer) {
		Stepper *timerID = (Stepper *)pvTimerGetTimerID(pxTimer);
		xSemaphoreGive(timerID->xSemaphoreTimeOut);
	}

	void AddProgramStep(double Speed_mm_h, uint32_t Time, bool stop);
	void inline DirectionUp() {HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);}
	void inline DirectionDown() {HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);}

private:
	void inline Enable() {HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);}
	void inline Disable() {HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);}

	TIM_HandleTypeDef htim8;	// TIM8_CH1
	TIM_OC_InitTypeDef hoc1;

	TimerHandle_t lTimer;

	QueueSetHandle_t xQueueSet;
	QueueHandle_t xQueueAddStep;
	SemaphoreHandle_t xSemaphoreTimeOut;
	std::list<StepperProgram_t> listProgram;

	bool isStoped;
};



#endif /* STEPPER_H_ */
