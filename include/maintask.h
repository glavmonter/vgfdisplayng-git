/*
 * maintask.h
 *
 *  Created on: 16 окт. 2014 г.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */

#ifndef MAINTASK_H_
#define MAINTASK_H_

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include <timers.h>
#include "stm32f4xx_hal.h"
#include "common.h"
#include "max6954.h"
#include "lmp90100.h"
#include "iadc.h"
#include "stepper.h"
#include "canlib.h"

void MainTaskStart();

class MainTask : public TaskBase {
public:
	MainTask(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth);

	~MainTask();

	void task();

	static void task_main(void *param) {
		static_cast<MainTask *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	SemaphoreHandle_t xSemaphoreTimeout;

private:
	CANLib 		*canDriver;
	MAX6954 	*max6954;

	LMP90100 	*lmp90100;
	IADC		*iAdc;

	Stepper 	*stepper;

	TimerHandle_t xTimer_1s;

	QueueSetHandle_t xQueueSet;

	uint32_t currentSpeed;
	double speeds[12] = {0.0, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 1.0, 2.0, 3.0, 5.0};
};

#endif /* MAINTASK_H_ */
