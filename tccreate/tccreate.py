#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys

if (len(sys.argv) < 3):
    print >> sys.stderr, "Using tccreate.py table_name filename"
    exit()

try:
    fin = open(sys.argv[2])
    lines = fin.readlines()
    fin.close()
except IOError as e:
    print "IO error({0}): ({1})".format(e.errno, e.strerror)
    exit()
    
table_size = 0

# Формируем из всего этого словарь
dict_uv = {}
for l in lines:
    split_line = l.split()
    if (len(split_line) > 11):
        del split_line[-1]
    temperature_begin = int(split_line[0])
    for T in range(len(split_line) - 1):
        dict_uv[temperature_begin + T] = int(float(split_line[T + 1])*1000.0)
        table_size += 1

next_line = 0
current_line = 0
string_table_uv = ""
string_table_T = ""

for key, value in sorted(dict_uv.iteritems(), key = lambda(k, v):(v,k)):
    if (current_line == table_size - 1):
        string_table_uv += "%u" % (int(value))
        string_table_T += "%u" % (int(key))
    else:
        string_table_uv += "%u, " % (int(value))
        string_table_T += "%u, " % (int(key))
                 
    next_line += 1
    current_line += 1
    if (next_line == 10):
        string_table_uv += "\n\t"
        string_table_T += "\n\t"
        next_line = 0

name_upper = sys.argv[1]
name_upper = name_upper.upper() + "_H_"

str_begin = "#ifndef %s\n#define %s\n\n" % (name_upper, name_upper)
str_begin += "const uint32_t %s_uV[] = {\n\t" % (sys.argv[1])

out_str = str_begin + string_table_uv + "\n\t};\n"
out_str += "\n\nconst uint32_t %s_C[] = {\n\t" % (sys.argv[1])
out_str += string_table_T + "\n\t};\n\n#endif"

print out_str
