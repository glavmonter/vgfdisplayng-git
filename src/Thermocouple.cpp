/*
 * Thermocouple.cpp
 *
 *  Created on: 16 июля 2014 г.
 *      Author: margel
 */


#include <FreeRTOS.h>
#include <stm32f4xx_hal.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include "Thermocouple.h"
#include "typeK.h"
#include "typeB.h"

static int32_t binsearch(const uint32_t *array, uint32_t array_size, uint32_t x) {
uint32_t first = 0;
uint32_t last = array_size;
	if (array_size == 0) {
		return -1;
	} else if (array[0] > x) {
		return -2;
	} else if (array[array_size - 1] < x) {
		return -3;
	}

	while (first < last) {
		uint32_t mid = first + (last - first)/2;
		if (x <= array[mid])
			last = mid;
		else
			first = mid + 1;
	}

	if (array[last] == x) {
		return last;
	} else {
		return last;
	}
}

float GetTemperature(uint32_t uV) {
#define TABLE_SIZE (sizeof(TypeK_uV)/sizeof(TypeK_uV[0]))

int32_t index;

	/* Возвращает индекс элемента или индекс ближайщего старшего */
	index = binsearch(TypeK_uV, TABLE_SIZE, uV);
	if (index < 0)
		return NAN;

	if (index == 0)
		return 0.0f;

float T_lower = (float)TypeK_C[index - 1];
float T_upper = (float)TypeK_C[index];
float uV_lower = (float)TypeK_uV[index - 1];
float uV_upper = (float)TypeK_uV[index];

float T = T_lower + ((float)uV - uV_lower)/(uV_upper - uV_lower)*(T_upper-T_lower);

	return T;
}
