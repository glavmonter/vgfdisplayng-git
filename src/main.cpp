//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>

#include <stm32f4xx_hal.h>
#include "diag/Trace.h"
#include "SEGGER_RTT.h"
#include "tlsf.h"
#include "config.h"
#include "main.h"
#include "maintask.h"
#include "hardware.h"
#include "canlib.h"

#define POOL_CCM_SIZE 	64*1024
#define POOL_RAM_SIZE	configTOTAL_HEAP_SIZE

portTASK_FUNCTION_PROTO(vRunStat, pvParameters);
static void prvSetupHardware();

#if configGENERATE_RUN_TIME_STATS == 1
volatile unsigned long ulRunTimeStatsClock;
static void prvInitRunTimeTimer();
#endif

__attribute__ ((section(".bss_CCMRAM")))
char pool_ccm[POOL_CCM_SIZE];
char pool_ram[POOL_RAM_SIZE];

int main(int argc, char* argv[]) {
(void) argc;
(void) argv;
	HAL_RCC_GetHCLKFreq();

	init_memory_pool(POOL_RAM_SIZE, pool_ram);
	prvSetupHardware();

#ifdef DEBUG
//	SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);
//	SEGGER_RTT_printf(0, "Hello world\n");
#endif

#if configGENERATE_RUN_TIME_STATS == 1
	prvInitRunTimeTimer();
#endif

	MainTaskStart();

	xTaskCreate(vRunStat, "Stat", configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY, NULL);
	vTaskStartScheduler();
}

/*
 * Настроим общую периферию (светодиоды, и процую херню)
 */
static void prvSetupHardware() {
GPIO_InitTypeDef GPIOInitStructure;
	__GPIOE_CLK_ENABLE();
	__GPIOD_CLK_ENABLE();

	/* LED0 - PE13 */
	GPIOInitStructure.Pin = GPIO_PIN_13;
	GPIOInitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIOInitStructure.Pull = GPIO_NOPULL;
	GPIOInitStructure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOE, &GPIOInitStructure);
	LED0_OFF();

	/* LED1 - PE12 */
	GPIOInitStructure.Pin = GPIO_PIN_12;
	HAL_GPIO_Init(GPIOE, &GPIOInitStructure);
	LED1_OFF();

	/* LED2 - PE11 */
	GPIOInitStructure.Pin = GPIO_PIN_11;
	HAL_GPIO_Init(GPIOE, &GPIOInitStructure);
	LED2_OFF();

	/* BUZZ - PD1 */
	GPIOInitStructure.Pin = GPIO_PIN_1;
	HAL_GPIO_Init(GPIOD, &GPIOInitStructure);
}


#if configGENERATE_RUN_TIME_STATS == 1
static TIM_HandleTypeDef 	htim7;
static void prvInitRunTimeTimer() {
	__TIM7_CLK_ENABLE();

	htim7.Instance 				= TIM7;
	htim7.Init.Prescaler 		= 0;
	htim7.Init.Period 			= (uint32_t)((SystemCoreClock/20000) - 1.0);
	htim7.Init.ClockDivision	= 0;
	htim7.Init.CounterMode 		= TIM_COUNTERMODE_UP;
	HAL_TIM_PWM_Init(&htim7);

	HAL_NVIC_SetPriority((IRQn_Type)TIM7_IRQn, 0x02, 0x00);
	HAL_NVIC_EnableIRQ((IRQn_Type)TIM7_IRQn);

	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	HAL_TIM_Base_Start_IT(&htim7);
}
#endif


#if configGENERATE_RUN_TIME_STATS == 1
char buffer[40*20];
#endif

portTASK_FUNCTION(vRunStat, pvParameters) {
(void) pvParameters;

	for(;;) {
#if configGENERATE_RUN_TIME_STATS == 1
		vTaskGetRunTimeStats(buffer);
		/* TODO выполнить альтернативную функцию vTaskGetRunTimeStats() */
		trace_printf("\n############################################\n");
		trace_printf(buffer);
		trace_printf("############################################\n");
#endif
		LED1_TOGGLE();
		vTaskDelay(5000);
	}
}

#if configGENERATE_RUN_TIME_STATS == 1
void TIM7_IRQHandler() {
	if (__HAL_TIM_GET_ITSTATUS(&htim7, TIM_IT_UPDATE)) {
		__HAL_TIM_CLEAR_IT(&htim7, TIM_IT_UPDATE);
		ulRunTimeStatsClock++;
	}
}
#endif

void vApplicationMallocFailedHook( void ) {
	trace_printf("Malloc failed!!!\n");
	for (;;){}
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName) {
	trace_printf("StackOverflow: 0x%X, %s\n", xTask, pcTaskName);
	LED0_ON();
	LED1_ON();
	LED2_ON();
	for (;;){}
}

// ----------------------------------------------------------------------------
