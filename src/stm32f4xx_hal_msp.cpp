/**
  ******************************************************************************
  * @file    SPI/SPI_FullDuplex_ComDMA/Src/stm32f4xx_hal_msp.c
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    26-June-2014
  * @brief   HAL MSP module.    
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************  
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/** @addtogroup STM32F4xx_HAL_Examples
  * @{
  */

/** @defgroup SPI_FullDuplex_ComDMA
  * @brief HAL MSP module.
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @defgroup HAL_MSP_Private_Functions
  * @{
  */

/**
  * @brief SPI MSP Initialization 
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param hspi: SPI handle pointer
  * @retval None
  */

static void HAL_SPI1_LMP90100_MspInit(SPI_HandleTypeDef *hspi) {
	__GPIOA_CLK_ENABLE();
	__GPIOC_CLK_ENABLE();

GPIO_InitTypeDef GPIOInitStructure;
	/* SPI1_NSS PA4 */
	GPIOInitStructure.Pin = GPIO_PIN_4;
	GPIOInitStructure.Pull = GPIO_NOPULL;
	GPIOInitStructure.Speed = GPIO_SPEED_HIGH;
	GPIOInitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);

	/* SPI1_SCK PA5 */
	GPIOInitStructure.Pin = GPIO_PIN_5;
	GPIOInitStructure.Pull = GPIO_NOPULL;
	GPIOInitStructure.Speed = GPIO_SPEED_HIGH;
	GPIOInitStructure.Mode = GPIO_MODE_AF_PP;
	GPIOInitStructure.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);

	/* SPI1_MISO PA6 */
	GPIOInitStructure.Pin = GPIO_PIN_6;
	GPIOInitStructure.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);

	/* SPI1_MOSI PA7 */
	GPIOInitStructure.Pin = GPIO_PIN_7;
	GPIOInitStructure.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);

	/* SPI1 DRDY, PC4 */
	GPIOInitStructure.Pin = GPIO_PIN_4;
	GPIOInitStructure.Mode = GPIO_MODE_IT_FALLING;
	GPIOInitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIOInitStructure);

	HAL_NVIC_SetPriority((IRQn_Type)EXTI4_IRQn, 0x0E, 0x0F);
	HAL_NVIC_EnableIRQ((IRQn_Type)EXTI4_IRQn);

	/* Enable SPI2 clock */
	__SPI1_CLK_ENABLE();
}

/*
 * Настройка выводов для SPI2, на котором MAX6954
 */
static void HAL_SPI2_MAX6954_MspInit(SPI_HandleTypeDef *hspi) {
(void)hspi;

	/* Включаем тактирование PB12-PB15 и PD3 */
	__GPIOB_CLK_ENABLE();
	__GPIOD_CLK_ENABLE();

GPIO_InitTypeDef GPIOInitStructure;

	/* SPI2_NSS - PB12 */
	GPIOInitStructure.Pin = GPIO_PIN_12;
	GPIOInitStructure.Pull = GPIO_NOPULL;
	GPIOInitStructure.Speed = GPIO_SPEED_HIGH;
	GPIOInitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	HAL_GPIO_Init(GPIOB, &GPIOInitStructure);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);

	/* SPI2_SCK - PB13 */
	GPIOInitStructure.Pin = GPIO_PIN_13;
	GPIOInitStructure.Pull = GPIO_NOPULL;
	GPIOInitStructure.Speed = GPIO_SPEED_HIGH;
	GPIOInitStructure.Mode = GPIO_MODE_AF_PP;
	GPIOInitStructure.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIOInitStructure);

	/* SPI2_MISO - PB14 */
	GPIOInitStructure.Pin = GPIO_PIN_14;
	GPIOInitStructure.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIOInitStructure);

	/* SPI2_MOSI - PB15 */
	GPIOInitStructure.Pin = GPIO_PIN_15;
	GPIOInitStructure.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIOInitStructure);

	/* SPI2_KIRQ - PD3 */
	GPIOInitStructure.Pin = GPIO_PIN_3;
	GPIOInitStructure.Mode = GPIO_MODE_IT_FALLING;
	GPIOInitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOD, &GPIOInitStructure);

	HAL_NVIC_SetPriority((IRQn_Type)EXTI3_IRQn, 0x0E, 0x0F);
	HAL_NVIC_EnableIRQ((IRQn_Type)EXTI3_IRQn);

	__SPI2_CLK_ENABLE();
}

void HAL_SPI3_MspInit(SPI_HandleTypeDef *hspi) {
}

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
	if (hspi->Instance == SPI1)
		HAL_SPI1_LMP90100_MspInit(hspi);
	else if (hspi->Instance == SPI2)
		HAL_SPI2_MAX6954_MspInit(hspi);
	else if (hspi->Instance == SPI3)
		HAL_SPI3_MspInit(hspi);
}

/**
  * @brief SPI MSP De-Initialization 
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO, DMA and NVIC configuration to their default state
  * @param hspi: SPI handle pointer
  * @retval None
  */
void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi)
{
}


/* Только измерение температуры кристалла */
static void HAL_ADC1_CPU_MspInit(ADC_HandleTypeDef* hadc) {
static DMA_HandleTypeDef hdma_adc;
	__DMA2_CLK_ENABLE();

	hdma_adc.Instance = DMA2_Stream0;

	hdma_adc.Init.Channel = DMA_CHANNEL_0;
	hdma_adc.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_adc.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_adc.Init.MemInc = DMA_MINC_ENABLE;
	hdma_adc.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_adc.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_adc.Init.Mode = DMA_CIRCULAR;
	hdma_adc.Init.Priority = DMA_PRIORITY_LOW;
	hdma_adc.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	hdma_adc.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_HALFFULL;
	hdma_adc.Init.MemBurst = DMA_MBURST_SINGLE;
	hdma_adc.Init.PeriphBurst = DMA_PBURST_SINGLE;
	HAL_DMA_Init(&hdma_adc);

	__HAL_LINKDMA(hadc, DMA_Handle, hdma_adc);

	HAL_NVIC_SetPriority((IRQn_Type)DMA2_Stream0_IRQn, 0x0E, 0x0F);
	HAL_NVIC_EnableIRQ((IRQn_Type)DMA2_Stream0_IRQn);
}

/*
 * Настройка ADC2 для считывания данных по току и напряжению
 */
static void HAL_ADC2_PWR_MspInit(ADC_HandleTypeDef* hadc) {

GPIO_InitTypeDef GPIOInitStructure;
	__GPIOA_CLK_ENABLE();

	/* PA0 - IN0 - I_P */
	GPIOInitStructure.Pin = GPIO_PIN_0;
	GPIOInitStructure.Mode = GPIO_MODE_ANALOG;
	GPIOInitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);

	/* PA1 - IN1 - I_N */
	GPIOInitStructure.Pin = GPIO_PIN_1;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);

	/* PA2 - IN2 - V_N */
	GPIOInitStructure.Pin = GPIO_PIN_2;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);

	/* PA3 - IN3 - V_P */
	GPIOInitStructure.Pin = GPIO_PIN_3;
	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);

static DMA_HandleTypeDef hdma_adc;
	__DMA2_CLK_ENABLE();

	hdma_adc.Instance = DMA2_Stream2;

	hdma_adc.Init.Channel = DMA_CHANNEL_1;
	hdma_adc.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_adc.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_adc.Init.MemInc = DMA_MINC_ENABLE;
	hdma_adc.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_adc.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma_adc.Init.Mode = DMA_CIRCULAR;
	hdma_adc.Init.Priority = DMA_PRIORITY_HIGH;
	hdma_adc.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	hdma_adc.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_HALFFULL;
	hdma_adc.Init.MemBurst = DMA_MBURST_SINGLE;
	hdma_adc.Init.PeriphBurst = DMA_PBURST_SINGLE;
	HAL_DMA_Init(&hdma_adc);

	__HAL_LINKDMA(hadc, DMA_Handle, hdma_adc);

	HAL_NVIC_SetPriority((IRQn_Type)DMA2_Stream2_IRQn, 0x0E, 0x0F);
	HAL_NVIC_EnableIRQ((IRQn_Type)DMA2_Stream2_IRQn);
}

void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc) {
	if (hadc->Instance == ADC1)
		HAL_ADC1_CPU_MspInit(hadc);
	else if (hadc->Instance == ADC2)
		HAL_ADC2_PWR_MspInit(hadc);

}

/*
 * Настройка вывода таймера для тактирования LMP90100
 */
void HAL_TIM_OC_MspInit(TIM_HandleTypeDef *htim) {
	if (htim->Instance != TIM1)
		return;

GPIO_InitTypeDef GPIOInitStructure;

	__GPIOB_CLK_ENABLE();
	__TIM1_CLK_ENABLE();

	/* TIM1_CH2N - PB0 */
	GPIOInitStructure.Pin = GPIO_PIN_0;
	GPIOInitStructure.Mode = GPIO_MODE_AF_PP;
	GPIOInitStructure.Pull = GPIO_PULLUP;
	GPIOInitStructure.Speed = GPIO_SPEED_HIGH;
	GPIOInitStructure.Alternate = GPIO_AF1_TIM1;
	HAL_GPIO_Init(GPIOB, &GPIOInitStructure);
}


void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim) {
GPIO_InitTypeDef GPIOInitStructure;

	__GPIOC_CLK_ENABLE();
	__TIM8_CLK_ENABLE();

	/* TIM8_CH1 - PC6 */
	GPIOInitStructure.Pin = GPIO_PIN_6;
	GPIOInitStructure.Mode = GPIO_MODE_AF_PP;
	GPIOInitStructure.Pull = GPIO_PULLUP;
	GPIOInitStructure.Speed = GPIO_SPEED_HIGH;
	GPIOInitStructure.Alternate = GPIO_AF3_TIM8;
	HAL_GPIO_Init(GPIOC, &GPIOInitStructure);
}


void HAL_CAN_MspInit(CAN_HandleTypeDef* hcan) {
GPIO_InitTypeDef GPIOInitStructure;
	if (hcan->Instance != CAN1)
		return;

	__GPIOB_CLK_ENABLE();
	__CAN1_CLK_ENABLE();

	/* CAN1_Tx - PB8 */
	GPIOInitStructure.Pin = GPIO_PIN_8 | GPIO_PIN_9;
	GPIOInitStructure.Mode = GPIO_MODE_AF_PP;
	GPIOInitStructure.Speed = GPIO_SPEED_FAST;
	GPIOInitStructure.Pull = GPIO_PULLUP;
	GPIOInitStructure.Alternate = GPIO_AF9_CAN1;
	HAL_GPIO_Init(GPIOB, &GPIOInitStructure);

	/* CAN1_Rx - PA11 */
//	GPIOInitStructure.Pin = GPIO_PIN_11;
//	HAL_GPIO_Init(GPIOA, &GPIOInitStructure);
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
