/*
 * lmp90100.c
 *
 *  Created on: 01 сент. 2014 г.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */


#include <stdint.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "tlsf.h"
#include "lmp90100.h"
#include "SEGGER_RTT.h"
#include "config.h"
#include "common.h"
#include "hardware.h"
#include "Thermocouple.h"

static LMP90100 *LMPAdc = NULL;

void LMPLink(LMP90100 *ll) {
	assert_param(ll != NULL);
	LMPAdc = ll;
}

void LMPInit() {
	LMPAdc = new LMP90100("LMP", configPRIORITY_ADC, configMINIMAL_STACK_SIZE*2);
}

/* Обработчики прерывания от DRDY */
void EXTI4_IRQHandler(void) {
static signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_4) != RESET) {
		__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_4);
		if ((LMPAdc != NULL) && (LMPAdc->xSemaphoreADC != NULL))
			xSemaphoreGiveFromISR(LMPAdc->xSemaphoreADC, &xHigherPriorityTaskWoken);
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/* Сама реализация классов */
LMP90100::LMP90100(char const *name, unsigned portBASE_TYPE priority, uint16_t stackDepth):
		xSemaphoreADC(NULL), m_uURA(LMP90100_URA_END), Vs_uV(5000000.0), Vs_Valid(false), Error(0.0),
		StatePressure(PRESSURE_NOT_CALIBRATED), Vo_uV(800000.0), Vo_Valid(false) {


	/* Тут настраиваем тактирование АЦП */
	htim.Instance = TIM1;
	htim.Init.Prescaler = 0;
	htim.Init.Period = SystemCoreClock/3571700 - 1;
	htim.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim.Init.CounterMode = TIM_COUNTERMODE_UP;
	HAL_TIM_OC_Init(&htim);

TIM_OC_InitTypeDef sConfig;
	sConfig.OCMode = TIM_OCMODE_PWM1;
	sConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfig.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	sConfig.OCIdleState = TIM_OCIDLESTATE_SET;
	sConfig.OCNIdleState = TIM_OCNIDLESTATE_SET;
	sConfig.OCFastMode = TIM_OCFAST_DISABLE;
	sConfig.Pulse = htim.Init.Period / 2;

	HAL_TIM_PWM_ConfigChannel(&htim, &sConfig, TIM_CHANNEL_2);
	HAL_TIMEx_OCN_Start(&htim, TIM_CHANNEL_2);

	/* Тут настраиваем SPI для АЦП */
	hspi.Instance = SPI1;
	hspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	hspi.Init.Direction = SPI_DIRECTION_2LINES;
	hspi.Init.CLKPhase = SPI_PHASE_2EDGE;
	hspi.Init.CLKPolarity = SPI_POLARITY_HIGH;
	hspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
	hspi.Init.CRCPolynomial = 7;
	hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi.Init.NSS = SPI_NSS_SOFT;
	hspi.Init.TIMode = SPI_TIMODE_DISABLED;
	hspi.Init.Mode = SPI_MODE_MASTER;
	HAL_SPI_Init(&hspi);


	/* Семафор для прерывания по DRDY */
	xSemaphoreADC = xSemaphoreCreateBinary();
	xSemaphoreTake(xSemaphoreADC, 0);

	/* Мьютекс для обновления данных АЦП */
	xMutex = xSemaphoreCreateBinary();
	xSemaphoreGive(xMutex);

	/* Очередь для запуска процесса калибровки */
	xQueueCalibrate = xQueueCreate(2, sizeof(int32_t));

	xQueueSet = xQueueCreateSet(1 + 2);
	xQueueAddToSet(xSemaphoreADC, xQueueSet);
	xQueueAddToSet(xQueueCalibrate, xQueueSet);

	filterPressure = new FilterKalman(0.1, 0.01, 0.0, 700.0, 0.0);
	xTaskCreate(&task_adc, name, stackDepth, this, priority, &handle);
}

LMP90100::~LMP90100() {
	vTaskDelete(handle);
}

#define CH_DATA_SIZE 	5	// 5 bytes: Status, ADC_DOUT2, ADC_DOUT1, ADC_DOUT0, CRC
void LMP90100::task() {
QueueSetMemberHandle_t activateMember;

//	SEGGER_RTT_printf(0, "Handle ADC: 0x%X\n", handle);
	this->SetCalibrationValue(19785.994112701417);
	WriteReg(0x00, 0xC3);
	WriteRegSettings();

	uint8_t addr;
	addr = LMP90100_SENDIAG_FLAGS_REG;
	EnableDataFirstMode(addr, CH_DATA_SIZE);

	for (;;) {
		activateMember = xQueueSelectFromSet(xQueueSet, portMAX_DELAY);
		if (activateMember == xSemaphoreADC) {
			xSemaphoreTake(xSemaphoreADC, 0);
			uint8_t data[CH_DATA_SIZE];

			NSS_LOW();
			HAL_SPI_Receive(&hspi, data, CH_DATA_SIZE, 10);
			NSS_HIGH();

			if (SPICRCCheck(data, CH_DATA_SIZE-1) == CRC_PASS) {
				int32_t adc_data = ((uint32_t)data[1] << 16) | ((uint32_t)data[2] << 8) | (uint32_t)data[3];

				uint8_t ch_num = data[0] & LMP90100_CH_NUM_MASK;
				uint8_t sendiag = data[0] & 0xF8;

				adc_data &= 0xFFFFFFF0;

				if (adc_data & 0x800000)
					adc_data = -(((~adc_data)&0xFFFFFF)+1);

				if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
					if (ch_num < 2) {
						Tc_uV[ch_num] = ((double)adc_data * 2500000.0)/(8388608.0*64.0);
						Tc_diag[ch_num] = sendiag;

					} else {
						ProcessPressure(ch_num, sendiag, adc_data);
					}

					LED0_TOGGLE();
					xSemaphoreGive(xMutex);
				}
			}
		} else if (activateMember == xQueueCalibrate) {
			uint32_t queueData;
			xQueueReceive(xQueueCalibrate, (void *)&queueData, 0);

			if (queueData == CALIBRATING_START) {
				StatePressure = PRESSURE_START_CALIBRATING;
				Error = 0.0;
			}
			else if (queueData == CALIBRATING_STOP) {
				StatePressure = PRESSURE_NOT_CALIBRATED;
			}
		}
	}
}

void LMP90100::ProcessPressure(uint8_t ch_num, uint8_t sendiag, int32_t ch_data) {
	if (ch_num == 2) { /* Напряжение питания датчика */
		Vs_uV = ((double)ch_data * 2500000.0)/(8388608.0*2.0) * 5.67774;
		Vs_V = Vs_uV / 1000000.0;
		Vs_diag = sendiag;
		Vs_Valid = true;

	} else if (ch_num == 3) { /* Выходное напряжение датчика */
		Vo_uV = ((double)ch_data * 2500000.0)/(8388608.0*1.0) * 1.9966;
		Vo_diag = sendiag;
		Vo_Valid = true;
	}

	/* P = K^-1*((Vo + Err)/Vs - 0.04), K = 0.0012858 */
	if (Vo_Valid) {
		Vo_Valid = false;
		Pressure_kPa = (1.0/0.0012858)*((Vo_uV + Error)/Vs_uV - 0.04);
		Pressure_kPa = filterPressure->Process(Pressure_kPa);

		if ((StatePressure == PRESSURE_START_CALIBRATING) or
			(StatePressure == PRESSURE_PROCESS_CALIBRATING)) {

			ProcessCalibrate(Pressure_kPa);
			return;
		}
	}
}

bool LMP90100::GetAdcData(double *ch0, double *ch1) {
bool ret = false;

	if ((ch0 == NULL) and (ch1 == NULL))
		return false;

	if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
		if (ch0)
			*ch0 = Tc_uV[0];
		if (ch1)
			*ch1 = Tc_uV[1];

		ret = true;
		xSemaphoreGive(xMutex);
	}
	return ret;
}

STATE_PRESSURE LMP90100::GetCalibratedStatus() {
STATE_PRESSURE ret = PRESSURE_NOT_CALIBRATED;
	if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
		ret = StatePressure;
		xSemaphoreGive(xMutex);
	}
	return ret;
}

bool LMP90100::GetCalibrationValue(double *err) {
bool ret = false;

	if (err == NULL)
		return false;

	if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
		ret = true;
		*err = Error;
		xSemaphoreGive(xMutex);
	}
	return ret;
}

bool LMP90100::SetCalibrationValue(double err) {
bool ret = false;
	if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
		Error = err;
		StatePressure = PRESSURE_CALIBRATED;
		ret = true;
		xSemaphoreGive(xMutex);
	}
	return ret;
}

void LMP90100::StartCalibration() {
uint32_t dout = CALIBRATING_START;
	xQueueSend(xQueueCalibrate, (void *)&dout, 10);
}

void LMP90100:: StopCalibration() {
uint32_t dout = CALIBRATING_STOP;
	xQueueSend(xQueueCalibrate, (void *)&dout, 10);
}

STATE_PRESSURE LMP90100::ProcessCalibrate(double Pressure) {
static TickType_t end_tick_of_stabilization = 0;
static TickType_t end_tick_of_calibration = 0;

static STATE_CALIBRATION state = CALIB_STABILIZATION;
static double Pressure_of_Start_Stabilization;

static double Average_Pressure  = 0.0;
static double Average_Vo = 0.0;
static double Average_Vs = 0.0;
static uint32_t NumPoints = 0;

static double LastPressure = 0.0;

	if (StatePressure == PRESSURE_START_CALIBRATING) {
		/* Инициализация начальных переменых для калибровки */
		end_tick_of_stabilization = xTaskGetTickCount() + STABILIZATION_TIME; /* Начальная стабилизаци - 5 секунд */

		LastPressure = Pressure;
		Pressure_of_Start_Stabilization = Pressure;

		StatePressure = PRESSURE_PROCESS_CALIBRATING;
		state = CALIB_STABILIZATION;

		return StatePressure;
	}

	if (state == CALIB_STABILIZATION) {
		if (xTaskGetTickCount() >= end_tick_of_stabilization) {
			/* Закончили стабилизацию, проверим на полное изменение давления */
			if (fabs(Pressure - Pressure_of_Start_Stabilization) > 0.200) {
				/* Давление изменилось слишком сильно с начала стабилизации */

				state = CALIB_STABILIZATION;
				StatePressure = PRESSURE_NOT_CALIBRATED;
				return StatePressure;
			} else {
				/* Начинаем считать среднее значение */
				Average_Pressure = 0.0;
				Average_Vo = 0.0;
				Average_Vs = 0.0;
				NumPoints = 0;

				/* Калибруем 5 секунд*/
				end_tick_of_calibration = xTaskGetTickCount() + CALIBRATION_TIME;

				state = CALIB_CALIBRATION;
				StatePressure = PRESSURE_PROCESS_CALIBRATING;
				return StatePressure;
			}
		} else {
			/* Тут идет стабилизация */
			if (fabs(LastPressure - Pressure) > 0.200) {
				state = CALIB_STABILIZATION;
				StatePressure = PRESSURE_NOT_CALIBRATED;
				return StatePressure;
			} else {
				LastPressure = Pressure;
				return StatePressure;
			}
		}
	} else if (state == CALIB_CALIBRATION) {
		if (xTaskGetTickCount() > end_tick_of_calibration) {

			/* Закончили калибровку, расчитаваем значение ошибки */
			Average_Pressure /= (double)NumPoints;
			Average_Vo /= (double)NumPoints;
			Average_Vs /= (double)NumPoints;

			Error = 0.04*Average_Vs - Average_Vo;

			state = CALIB_STABILIZATION;
			StatePressure = PRESSURE_CALIBRATED;
			return StatePressure;

		} else {
			/* Считаем среднее */
			Average_Pressure += Pressure;
			Average_Vo += Vo_uV;
			Average_Vs += Vs_uV;
			NumPoints++;
			return StatePressure;
		}
	}

	return StatePressure;
}


bool LMP90100::GetPressure_kPa(double *Pressure) {
bool ret = false;

	if (Pressure == NULL)
		return false;

	if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
		*Pressure = Pressure_kPa;
		ret = true;
		xSemaphoreGive(xMutex);
	}

	return ret;
}

/*
 * Запись значения во внутренний регистр АЦП
 */
void LMP90100::WriteReg(uint8_t addr, uint8_t value) {
uint8_t newURA, inst;

	newURA = (addr & LMP90100_URA_MASK) >> 4;
	NSS_LOW();

	if (m_uURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;
		HAL_SPI_Transmit(&hspi, &inst, 1, 1);
		HAL_SPI_Transmit(&hspi, &newURA, 1, 1);
		m_uURA = newURA;
	}

	inst = LMP90100_WRITE_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);
	HAL_SPI_Transmit(&hspi, &inst, 1, 1);
	HAL_SPI_Transmit(&hspi, &value, 1, 1);

	NSS_HIGH();
}

/*
 * Чтение значения из внутреннего регистра АЦП
 */
uint8_t LMP90100::ReadReg(uint8_t addr) {
uint8_t newURA, inst, rxdata;

	newURA = (addr & LMP90100_URA_MASK) >> 4;

	NSS_LOW();
	if (m_uURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;
		HAL_SPI_Transmit(&hspi, &inst, 1, 10);
		HAL_SPI_Transmit(&hspi, &newURA, 1, 10);

		m_uURA = newURA;
	}

	inst = LMP90100_READ_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);
	HAL_SPI_Transmit(&hspi, &inst, 1, 10);
	HAL_SPI_Receive(&hspi, &rxdata, 1, 10);

	NSS_HIGH();
	return rxdata;
}


/*
 * Все, что нужно для вычисления CRC
 */
uint8_t LMP90100::crc8MakeBitwise2(uint8_t crc, uint8_t poly, uint8_t *pmsg, uint8_t msg_size) {
uint8_t msg;
	for (int i = 0 ; i < msg_size ; i++) {
		msg = (*pmsg++ << 0);
		for (int j = 0 ; j < 8 ; j++) {
			if((msg ^ crc) >> 7)
				crc = (crc << 1) ^ poly;
			else
				crc <<= 1;
			msg <<= 1;
		}
	}
	return(crc ^ CRC8_FINAL_XOR);
}

uint8_t LMP90100::SPICRCCheck (uint8_t *buffer, uint8_t count) {
uint8_t crc_data, crc_calc;

	crc_data = buffer[count];                                                    // extract CRC data
	crc_calc = crc8MakeBitwise2(CRC8_INIT_REM, CRC8_POLY,
                                buffer, count);                     // calculate CRC for the adc output (size 3 bytes)
	if (crc_data == crc_calc)
		return CRC_PASS;
	else
		return CRC_FAIL;
}

/*
 * Грубая настройка АЦП
 */
void LMP90100::WriteRegSettings() {
	WriteReg(			LMP90100_RESETCN_REG,
						LMP90100_RESETCN_REG_VALUE);
	WriteReg(			LMP90100_SPI_HANDSHAKECN_REG,
						LMP90100_SPI_HANDSHAKECN_REG_VALUE);
	WriteReg(			LMP90100_SPI_STREAMCN_REG,
						LMP90100_SPI_STREAMCN_REG_VALUE);
	WriteReg(			LMP90100_PWRCN_REG,
						LMP90100_PWRCN_REG_VALUE);
	WriteReg(			LMP90100_ADC_RESTART_REG,
						LMP90100_ADC_RESTART_REG_VALUE);
	WriteReg(			LMP90100_GPIO_DIRCN_REG,
						LMP90100_GPIO_DIRCN_REG_VALUE);
	WriteReg(			LMP90100_GPIO_DAT_REG,
						LMP90100_GPIO_DAT_REG_VALUE);
	WriteReg(			LMP90100_BGCALCN_REG,
						LMP90100_BGCALCN_REG_VALUE);
	WriteReg(			LMP90100_SPI_DRDYBCN_REG,
						LMP90100_SPI_DRDYBCN_REG_VALUE);
	WriteReg(			LMP90100_ADC_AUXCN_REG,
						LMP90100_ADC_AUXCN_REG_VALUE);
	WriteReg(			LMP90100_SPI_CRC_CN_REG,
						LMP90100_SPI_CRC_CN_REG_VALUE);
	WriteReg(			LMP90100_SENDIAG_THLDH_REG,
						LMP90100_SENDIAG_THLDH_REG_VALUE);
	WriteReg(			LMP90100_SENDIAG_THLDL_REG,
						LMP90100_SENDIAG_THLDL_REG_VALUE);
	WriteReg(			LMP90100_SCALCN_REG,
						LMP90100_SCALCN_REG_VALUE);
	WriteReg(			LMP90100_ADC_DONE_REG,
						LMP90100_ADC_DONE_REG_VALUE);

TickType_t nextTick;
	nextTick = xTaskGetTickCount() + 1000; // Не более 1000 тиков читаем статус DAC
	while ((ReadReg(LMP90100_CH_STS_REG & LMP90100_CH_SCAN_NRDY)) && (xTaskGetTickCount() < nextTick));

	WriteReg(			LMP90100_CH_SCAN_REG,
						LMP90100_CH_SCAN_REG_VALUE);

	/* Усиление по каналам */
	WriteReg(			LMP90100_CH0_INPUTCN_REG,
						LMP90100_CH0_INPUTCN_REG_VALUE);
	WriteReg(			LMP90100_CH0_CONFIG_REG,
						LMP90100_CH0_CONFIG_REG_VALUE);

	WriteReg(			LMP90100_CH1_INPUTCN_REG,
						LMP90100_CH1_INPUTCN_REG_VALUE);
	WriteReg(			LMP90100_CH1_CONFIG_REG,
						LMP90100_CH1_CONFIG_REG_VALUE);

	WriteReg(			LMP90100_CH2_INPUTCN_REG,
						LMP90100_CH2_INPUTCN_REG_VALUE);
	WriteReg(			LMP90100_CH2_CONFIG_REG,
						LMP90100_CH2_CONFIG_REG_VALUE);

	WriteReg(			LMP90100_CH3_INPUTCN_REG,
						LMP90100_CH3_INPUTCN_REG_VALUE);
	WriteReg(			LMP90100_CH3_CONFIG_REG,
						LMP90100_CH3_CONFIG_REG_VALUE);

	WriteReg(			LMP90100_CH4_INPUTCN_REG,
						LMP90100_CH4_INPUTCN_REG_VALUE);
	WriteReg(			LMP90100_CH4_CONFIG_REG,
						LMP90100_CH4_CONFIG_REG_VALUE);

	WriteReg(			LMP90100_CH5_INPUTCN_REG,
						LMP90100_CH5_INPUTCN_REG_VALUE);
	WriteReg(			LMP90100_CH5_CONFIG_REG,
						LMP90100_CH5_CONFIG_REG_VALUE);

	WriteReg(			LMP90100_CH6_INPUTCN_REG,
						LMP90100_CH6_INPUTCN_REG_VALUE);
	WriteReg(			LMP90100_CH6_CONFIG_REG,
						LMP90100_CH6_CONFIG_REG_VALUE);
}

/**
 * @function LMP90100_SPIEnableDataFirstMode
 * Enables the data first mode of LMP90100.
 */
void LMP90100::EnableDataFirstMode(uint8_t addr, uint8_t count) {
	WriteReg(LMP90100_DATA_ONLY_1_REG, addr);
	WriteReg(LMP90100_DATA_ONLY_2_REG, count - 1);

uint8_t txdata;
uint8_t rxdata;

	/* NSS enable */
	NSS_LOW();

	txdata = LMP90100_DATA_FIRST_MODE_INSTRUCTION_ENABLE;
	HAL_SPI_Transmit(&hspi, &txdata, 1, 10);

	txdata = LMP90100_DATA_FIRST_MODE_INSTRUCTION_READ_MODE_STATUS;
	HAL_SPI_TransmitReceive(&hspi, &txdata, &rxdata, 1, 10);

TickType_t nextTick = xTaskGetTickCount() + 1000;
	while ((rxdata & LMP90100_DATA_FIRST_MODE_STATUS_FLAG) && (xTaskGetTickCount() < nextTick)) {
		HAL_SPI_TransmitReceive(&hspi, &txdata, &rxdata, 1, 10);
	}

	/* NSS disable */
	NSS_HIGH();
}


/* Q - Скорость реакции */
FilterKalman::FilterKalman(double q, double r, double xEstLast, double PLast, double xEst)
	:Q(0.1), R(0.01), x_est_last(0.0), P_last(700.0), x_est(0.0) {

	Q = q;
	R = r;
	x_est_last = xEstLast;
	P_last = PLast;
	x_est = xEst;
}

FilterKalman::~FilterKalman() {
}

double FilterKalman::Process(double data_in) {
double x_temp_est;
double P_temp;
double K;

	x_temp_est = x_est_last;
	P_temp = P_last + Q;
	K = P_temp*(1.0/(P_temp + R));
	x_est = x_temp_est + K*(data_in - x_temp_est);

	P_last = (1.0 - K)*P_temp;
	x_est_last = x_est;

	return x_est;
}

