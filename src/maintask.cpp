/*
 * maintask.cpp
 *
 *  Created on: 16 окт. 2014 г.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */


#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <stdio.h>
#include "SEGGER_RTT.h"
#include "maintask.h"
#include "config.h"

void MainTaskStart() {
	MainTask *mainTask = new MainTask("Main", tskIDLE_PRIORITY, configMINIMAL_STACK_SIZE*2);
	(void)mainTask;
}

static void Timer_1s(TimerHandle_t xTimer) {
SemaphoreHandle_t timeout = (SemaphoreHandle_t)pvTimerGetTimerID(xTimer);
	xSemaphoreGive(timeout);
}


MainTask::MainTask(const char *name, unsigned portBASE_TYPE priority, uint16_t stackDepth)
	:currentSpeed(0){

	max6954 = new MAX6954("MAX", tskIDLE_PRIORITY + 1, configMINIMAL_STACK_SIZE*2);
	MAX6954Link(max6954);

	lmp90100 = new LMP90100("LMP", configPRIORITY_ADC, configMINIMAL_STACK_SIZE*2);
	LMPLink(lmp90100);

	iAdc = new IADC("IADC", tskIDLE_PRIORITY + 2, configMINIMAL_STACK_SIZE*2);
	IADCLink(iAdc);

	stepper = new Stepper("Stepper", tskIDLE_PRIORITY + 3, configMINIMAL_STACK_SIZE*2);

	canDriver = new CANLib("CAN", tskIDLE_PRIORITY + 4, 5, configMINIMAL_STACK_SIZE*3);
	CANLibLink(canDriver);

	canDriver->SetIAdc(iAdc);
	canDriver->Ping();

	xSemaphoreTimeout = xSemaphoreCreateBinary();
	xSemaphoreTake(xSemaphoreTimeout, 0);

	xQueueSet = xQueueCreateSet(1 + 1);
	xQueueAddToSet(xSemaphoreTimeout, xQueueSet);
	xQueueAddToSet(max6954->xQueueButtonEvent, xQueueSet);

	xTimer_1s = xTimerCreate("Timer_1s", 100, pdTRUE, (void *)xSemaphoreTimeout, Timer_1s);
	xTaskCreate(task_main, name, stackDepth, this, priority, &handle);
}

MainTask::~MainTask() {
	vTaskDelete(handle);
}

#define CALIBRATION_LETTERS	0x004E770E

#define MOTION_TIME			(1000*60*60)

void MainTask::task() {
double Pressure_mmHg;
DIRECTION direction = DIR_UP;

	max6954->PrintCustom(LINE_2, 0x40404040);

	max6954->PrintCustom(LINE_2, 0x40404040);

//	SEGGER_RTT_printf(0, "Main task: 0x%X\n", handle);
	xTimerStart(xTimer_1s, 0);
	xSemaphoreGive(xSemaphoreTimeout);

	max6954->PrintLine(LINE_1, (float)speeds[currentSpeed]);

	stepper->DirectionUp();

	for (;;) {
		QueueSetMemberHandle_t activateMember = xQueueSelectFromSet(xQueueSet, portMAX_DELAY);

		if (activateMember == xSemaphoreTimeout) {
			xSemaphoreTake(xSemaphoreTimeout, 0);

			float coretemp = iAdc->GetCPUTemperature();
			float PowerU = iAdc->GetPowerUmV() / 1000.0f;
			float PowerI = iAdc->GetPowerImA() / 1000.0f;

			lmp90100->GetPressure_kPa(&Pressure_mmHg);
			Pressure_mmHg = Pressure_mmHg * 1000.0 / 133.3;

			max6954->PrintLine(LINE_0, PowerU);
			max6954->PrintLine(LINE_1, PowerI);
			max6954->PrintLine(LINE_2, PowerI*PowerU);

			switch (lmp90100->GetCalibratedStatus()) {

			case PRESSURE_PROCESS_CALIBRATING:
				max6954->PrintCustom(LINE_3, CALIBRATION_LETTERS);
				break;

			case PRESSURE_CALIBRATED:
			case PRESSURE_NOT_CALIBRATED:
				max6954->PrintLine(LINE_3, (float)Pressure_mmHg);
				break;
			default:
				break;
			}
		} else if (activateMember == max6954->xQueueButtonEvent) {

			ButtonEvent event;
			xQueueReceive(max6954->xQueueButtonEvent, (void *)&event, 0);

			if (event.Button == 1) {
				lmp90100->StartCalibration();

			} else if (event.Button == 2) {
				/* Увеличиваем скорость */
				stepper->AddProgramStep(0.0, 0, true);
				if (currentSpeed < sizeof(speeds)/sizeof(double) - 1) {
					currentSpeed++;

					stepper->AddProgramStep(speeds[currentSpeed], MOTION_TIME, false);
//					max6954->PrintLine(LINE_1, (float)speeds[currentSpeed]);
				}

			} else if (event.Button == 3) {
				/* Уменьшаем скорость */
				stepper->AddProgramStep(0.0, 0, true);
				if (currentSpeed > 0) {
					currentSpeed--;
					if (currentSpeed == 0)
						stepper->AddProgramStep(0.0, 0, true);
					else
						stepper->AddProgramStep(speeds[currentSpeed], MOTION_TIME, false);

//					max6954->PrintLine(LINE_1, (float)speeds[currentSpeed]);
				}

			} else if (event.Button == 6) {
				/* Остановить мотор */
				currentSpeed = 0;
				stepper->AddProgramStep(0.0, 0, true);

//				max6954->PrintLine(LINE_1, (float)speeds[currentSpeed]);

			} else if (event.Button == 5) {
				switch (direction) {
				case DIR_UP:
					direction = DIR_DOWN;
//					max6954->PrintCustom(LINE_2, 0x08080808);
					stepper->DirectionDown();
					break;

				case DIR_DOWN:
					direction = DIR_UP;
//					max6954->PrintCustom(LINE_2, 0x40404040);
					stepper->DirectionUp();
					break;
				}
			}
		}
	}
}
