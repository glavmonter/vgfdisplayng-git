/*
 * iadc.cpp
 *
 *  Created on: 15 oct 2014.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */


#include <FreeRTOS.h>
#include <new>
#include <math.h>
#include "tlsf.h"
#include "common.h"
#include "iadc.h"
#include "stm32f4xx_hal.h"
#include "SEGGER_RTT.h"

static IADC *iadc = NULL;

void IADCLink(IADC *ll) {
	assert_param(ll != NULL);
	iadc = ll;
}

void IADCInit() {
	iadc = new IADC("IADC", tskIDLE_PRIORITY + 2, configMINIMAL_STACK_SIZE * 2);
}


/*
 * Номера каналов:
 * PA0 - ADC[1-3]_IN0 - I_P
 * PA1 - ADC[1-3]_IN1 - I_N
 * PA2 - ADC[1-3]_IN2 - V_N
 * PA3 - ADC[1-3]_IN3 - V_P
 *
 * ADC1_IN16 - Температура кристалла
 */

IADC::IADC(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth): current_bank(BANK_1) {

TIM_MasterConfigTypeDef sMasterConfig;

	__TIM3_CLK_ENABLE();
	htimADC1.Instance = TIM3;
	htimADC1.Init.Prescaler = 2625-1; 	// 84MHz /32kHz - 1, считает с частотой 32 кГц
	htimADC1.Init.Period = 399;			// Триггер 80 Гц. 32000/(399 + 1) = 80 Гц
	htimADC1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2;
	htimADC1.Init.CounterMode = TIM_COUNTERMODE_UP;
	HAL_TIM_Base_Init(&htimADC1);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&htimADC1, &sMasterConfig);

	/* ADC1 только измерение температуры кристалла */
	__ADC1_CLK_ENABLE();
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
	hadc1.Init.Resolution = ADC_RESOLUTION12b;
	hadc1.Init.ScanConvMode = ENABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.NbrOfDiscConversion = 0;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
	hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T3_TRGO;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 1;
	hadc1.Init.DMAContinuousRequests = ENABLE;
	hadc1.Init.EOCSelection = DISABLE;
	HAL_ADC_Init(&hadc1);

ADC_ChannelConfTypeDef sConfig;

	/* Канал температуры */
	sConfig.Channel = ADC_CHANNEL_16;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
	sConfig.Offset = 0;
	HAL_ADC_ConfigChannel(&hadc1, &sConfig);


	__TIM2_CLK_ENABLE();
	htimADC2.Instance = TIM2;
	htimADC2.Init.Prescaler = 1400 - 1; // 84 MHz/60kHz - 1. Считает с частотой 60 kHz
	htimADC2.Init.Period = 9;			// 60000/(9+1) = 6000 Гц
	htimADC2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2;
	htimADC2.Init.CounterMode = TIM_COUNTERMODE_UP;
	HAL_TIM_Base_Init(&htimADC2);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&htimADC2, &sMasterConfig);

	/* Измерение напряжения и тока силы */
	__ADC2_CLK_ENABLE();
	hadc2.Instance = ADC2;
	hadc2.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
	hadc2.Init.Resolution = ADC_RESOLUTION12b;
	hadc2.Init.ScanConvMode = ENABLE;
	hadc2.Init.ContinuousConvMode = DISABLE;
	hadc2.Init.DiscontinuousConvMode = DISABLE;
	hadc2.Init.NbrOfDiscConversion = 0;
	hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
	hadc2.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T2_TRGO;
	hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc2.Init.NbrOfConversion = 4;
	hadc2.Init.DMAContinuousRequests = ENABLE;
	hadc2.Init.EOCSelection = DISABLE;
	HAL_ADC_Init(&hadc2);


	/* I_P */
	sConfig.Channel = ADC_CHANNEL_0;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
	sConfig.Offset = 0;
	HAL_ADC_ConfigChannel(&hadc2, &sConfig);

	/* I_N */
	sConfig.Channel = ADC_CHANNEL_1;
	sConfig.Rank = 2;
	sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
	sConfig.Offset = 0;
	HAL_ADC_ConfigChannel(&hadc2, &sConfig);

	/* V_N */
	sConfig.Channel = ADC_CHANNEL_2;
	sConfig.Rank = 3;
	sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
	sConfig.Offset = 0;
	HAL_ADC_ConfigChannel(&hadc2, &sConfig);

	/* V_P */
	sConfig.Channel = ADC_CHANNEL_3;
	sConfig.Rank = 4;
	sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES;
	sConfig.Offset = 0;
	HAL_ADC_ConfigChannel(&hadc2, &sConfig);

	vSemaphoreCreateBinary(xSemaphoreISRADC1);
	xSemaphoreTake(xSemaphoreISRADC1, 1);

	vSemaphoreCreateBinary(xSemaphoreISRADC2);
	xSemaphoreTake(xSemaphoreISRADC2, 1);

	xQueueSet = xQueueCreateSet(1 + 1);
	xQueueAddToSet(xSemaphoreISRADC1, xQueueSet);
	xQueueAddToSet(xSemaphoreISRADC2, xQueueSet);

	xMutexTemperature = xSemaphoreCreateMutex();
	xMutexPowerData = xSemaphoreCreateMutex();

	xTaskCreate(task_iadc, name, stackDepth, this, priority, &handle);
}

IADC::~IADC() {
	vTaskDelete(this->handle);
}

/*
 * datas - Указатель на текущий обрабатываемый блок
 * size - размер обрабатываемого блока
 * Urms - указатель на выходное значение напряжения
 * Irms - указатель на выходное значение тока
 */
uint32_t IADC::CalcRMS(uint16_t *datas, uint32_t size, float *Urms, float *Irms) {
float sU = 0.0f;
float sI = 0.0f;

	if ((datas == NULL) || (Urms == NULL) || (Irms == NULL) || (size == 0) || (size % 4 != 0))
		return -1;

	for (uint32_t i = 0; i < size; i += 4) {
		float lI, lU;

		lI = (float)datas[i + 0] - (float)datas[i + 1];
		lU = (float)datas[i + 3] - (float)datas[i + 2];

		sU += lU * lU;
		sI += lI * lI;
	}

	sU /= (float)(size)/4.0f;
	sI /= (float)(size)/4.0f;

	sI = sqrtf(sI);
	sI = (sI*3300.0f/4095.0f) / 1.85f;
	sI = (float)((double)sI*5000.0/(0.000375*240000.0));	// Is, mA

	sU = sqrtf(sU);
	sU = (sU*3300.0f/4095.0f) * 11.0f;

	if (xSemaphoreTake(xMutexPowerData, 10) == pdTRUE) {
		*Urms = sU;
		*Irms = sI;
		xSemaphoreGive(xMutexPowerData);
	}
	return 0;
}

float IADC::GetCPUTemperature() {
float ret = -1.0f;
	if (xSemaphoreTake(xMutexTemperature, 10) == pdTRUE) {
		ret = TemperatureCPU;
		xSemaphoreGive(xMutexTemperature);
	}
	return ret;
}

float IADC::GetPowerUmV() {
float ret = -1.0f;
	if (xSemaphoreTake(xMutexPowerData, 10) == pdTRUE) {
		ret = this->PowerU;
		xSemaphoreGive(xMutexPowerData);
	}
	return ret;
}

float IADC::GetPowerImA() {
float ret = -1.0f;
	if (xSemaphoreTake(xMutexPowerData, 10) == pdTRUE) {
		ret = this->PowerI;
		xSemaphoreGive(xMutexPowerData);
	}
	return ret;
}

void IADC::GetPowerUI(float *UmV, float *ImA) {
	if ((UmV == NULL) && (ImA == NULL))
		return;

	if (xSemaphoreTake(xMutexPowerData, 10) == pdTRUE) {
		if (UmV)
			*UmV = PowerU;
		if (ImA)
			*ImA = PowerI;
		xSemaphoreGive(xMutexPowerData);
	}
}

void IADC::task() {
//	SEGGER_RTT_printf(0, "Handle IADC: 0x%X\n", handle);

	datas_tele_bank_1 = (uint16_t *)pvPortMalloc(sizeof(uint16_t) * NUM_TELEMETRY);
	datas_tele_bank_2 = (uint16_t *)pvPortMalloc(sizeof(uint16_t) * NUM_TELEMETRY);

	datas_temp = (uint16_t *)pvPortMalloc(sizeof(uint16_t) * NUM_TEMPERATURES);

	HAL_ADC_Start_DMA(&hadc1, (uint32_t *)datas_temp, NUM_TEMPERATURES);
	HAL_ADC_Start_DMA(&hadc2, (uint32_t *)datas_tele_bank_1, NUM_TELEMETRY);

	HAL_TIM_Base_Start(&htimADC1);
	HAL_TIM_Base_Start(&htimADC2);

QueueSetMemberHandle_t xActivateMember;

TickType_t tick_tele = 0;
TickType_t tick_temp = 0;

	for (;;) {
		xActivateMember = xQueueSelectFromSet(xQueueSet, portMAX_DELAY);
		if (xActivateMember == xSemaphoreISRADC1) {
			/* ADC1 */
			xSemaphoreTake(xSemaphoreISRADC1, 0);

			/* Новое значение температуры каждые 400 тиков */
			uint32_t averageT = 0;
			for (uint32_t i = 0; i < NUM_TEMPERATURES; i++)
				averageT += datas_temp[i];

			HAL_ADC_Start_DMA(&hadc1, (uint32_t *)datas_temp, NUM_TEMPERATURES);

			float T = (float)averageT/NUM_TEMPERATURES;
			T = ((T*3300/0xfff/1000.0f)-0.760f)/0.0025f+25.0f;
			if (xSemaphoreTake(xMutexTemperature, 10) == pdTRUE) {
				TemperatureCPU = T;
				xSemaphoreGive(xMutexTemperature);
			}

			uint32_t t0 = (uint32_t)T;
			uint32_t t1 = (uint32_t)((T - (float)t0)*10.0);

//			SEGGER_RTT_printf(0, "T core: %d.%d C, tick:%d\n", t0, t1, xTaskGetTickCount() - tick_temp);
			tick_temp = xTaskGetTickCount();

		} else if (xActivateMember == xSemaphoreISRADC2) {
			xSemaphoreTake(xSemaphoreISRADC2, 0);

			if (current_bank == BANK_1) {
				HAL_ADC_Start_DMA(&hadc2, (uint32_t *)datas_tele_bank_2, NUM_TELEMETRY);
				current_bank = BANK_2;

				/* Считаем RMS */
				CalcRMS(datas_tele_bank_1, NUM_TELEMETRY, &PowerU, &PowerI);

			} else if (current_bank == BANK_2) {
				HAL_ADC_Start_DMA(&hadc2, (uint32_t *)datas_tele_bank_1, NUM_TELEMETRY);
				current_bank = BANK_1;

				/* Считаем RMS */
				CalcRMS(datas_tele_bank_2, NUM_TELEMETRY, &PowerU, &PowerI);
			}

//			SEGGER_RTT_printf(0, "U: %d mV, I: %d mA, tick: %d\n", (int32_t)GetPowerUmV(), (int32_t)GetPowerImA(), xTaskGetTickCount() - tick_tele);
			tick_tele = xTaskGetTickCount();
		}
	}
}


void DMA2_Stream2_IRQHandler() {
	assert_param(iadc != NULL);
	HAL_DMA_IRQHandler(iadc->hadc2.DMA_Handle);
}

void DMA2_Stream0_IRQHandler() {
	assert_param(iadc != NULL);
	HAL_DMA_IRQHandler(iadc->hadc1.DMA_Handle);
}


void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle) {
static signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	if (AdcHandle->Instance == ADC1) {
		xSemaphoreGiveFromISR(iadc->xSemaphoreISRADC1, &xHigherPriorityTaskWoken);
		HAL_ADC_Stop_DMA(AdcHandle);
	} else if (AdcHandle->Instance == ADC2) {
		xSemaphoreGiveFromISR(iadc->xSemaphoreISRADC2, &xHigherPriorityTaskWoken);
		HAL_ADC_Stop_DMA(AdcHandle);
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


