/*
 * max6954.cpp
 *
 *  Created on: 14 oct 2014.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */


#include <FreeRTOS.h>
#include <new>
#include <stdio.h>
#include <string.h>
#include "tlsf.h"
#include "common.h"
#include "max6954.h"
#include "stm32f4xx_hal.h"
#include "SEGGER_RTT.h"

MAX6954 *max6954 = NULL;

void MAX6954Init() {
	max6954 = new MAX6954("MAX", tskIDLE_PRIORITY + 1, configMINIMAL_STACK_SIZE*2);
}

void MAX6954Link(MAX6954 *ll) {
	max6954 = ll;
}

/* Обработчики прерывания от DRDY */
void EXTI3_IRQHandler(void) {
static signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_3) != RESET) {
		__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_3);

		if ((max6954 != NULL) && (max6954->xSemaphoreISR != NULL))
			xSemaphoreGiveFromISR(max6954->xSemaphoreISR, &xHigherPriorityTaskWoken);
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

MAX6954::MAX6954(char const *name, unsigned portBASE_TYPE priority,
			uint16_t stackDepth) {

	hspi.Instance = SPI2;
	hspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	hspi.Init.Direction = SPI_DIRECTION_2LINES;
	hspi.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
	hspi.Init.CRCPolynomial = 7;
	hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi.Init.NSS = SPI_NSS_SOFT;
	hspi.Init.TIMode = SPI_TIMODE_DISABLED;
	hspi.Init.Mode = SPI_MODE_MASTER;
	HAL_SPI_Init(&hspi);

	xMutex = xSemaphoreCreateMutex();

	xSemaphoreISR = xSemaphoreCreateBinary();
	xSemaphoreTake(xSemaphoreISR, 0);

	xQueueButtonEvent = xQueueCreate(1, sizeof(ButtonEvent));

	xTaskCreate(task_max, name, stackDepth, this, priority, &handle);
}

MAX6954::~MAX6954() {
	vTaskDelete(this->handle);
}


void MAX6954::EnableDecodeMode() {
uint8_t tx[2] = {0x01, 0xFF};
	NSS_LOW();
	HAL_SPI_Transmit(&hspi, tx, 2, 100);
	NSS_HIGH();
}

void MAX6954::DisableDecodeMode() {
uint8_t tx[2] = {0x01, 0x00};
	NSS_LOW();
	HAL_SPI_Transmit(&hspi, tx, 2, 100);
	NSS_HIGH();
}

void MAX6954::EnableGlobalBrightness() {
uint8_t tx[2] = {0x02, 0x01};
	NSS_LOW();
	HAL_SPI_Transmit(&hspi, tx, 2, 100);
	NSS_HIGH();

	tx[0] = 0x04; tx[1] = 0b101;
	NSS_LOW();
	HAL_SPI_Transmit(&hspi, tx, 2, 100);
	NSS_HIGH();
}

void MAX6954::DisableGlobalBrightness() {
uint8_t tx[2] = {0x02, 0x00};
	NSS_LOW();
	HAL_SPI_Transmit(&hspi, tx, 2, 100);
	NSS_HIGH();

	tx[0] = 0x04; tx[1] = 0b01000101;
	NSS_LOW();
	HAL_SPI_Transmit(&hspi, tx, 2, 100);
	NSS_HIGH();
}

void MAX6954::SetGlobalBrightness(BRIGHTNESS brightness) {
	SendData(0x02, brightness);
}

void MAX6954::SelfTest() {
	SendData(0x04, 0x01); /* E */

	SendData(0x07, 0x01);
	vTaskDelay(250);
	SendData(0x07, 0x00);
	vTaskDelay(250);
	SendData(0x07, 0x01);
	vTaskDelay(250);
	SendData(0x07, 0x00);

	EnableDecodeMode();
	SetGlobalBrightness(BRIGHTNESS_5_16);

	for (uint8_t i = 0x20; i <= 0x2F; i++) {
		SendData(i, 0x20);
		SendData(i + 0x20, 0x20);
		SendData(i + 0x60, 0x20);
	}

	for (uint8_t i = 0x20; i <= 0x2F; i++) {
		SendData(i, i - 0x20);
	}

	vTaskDelay(500);
	for (uint8_t d = 0; d <= 15; d++) {
		for (uint8_t i = 0x20; i <= 0x2F; i++) {
			SendData(i, d | 0x80);
		}
		vTaskDelay(200);
	}

	vTaskDelay(500);
	for (uint8_t i = 0x20; i < 0x28; i++)
		SendData(i, 0x20);

	DisableDecodeMode();
	ClearAll();
}

void MAX6954::ClearAll() {
	for (uint8_t i = 0x60; i <= 0x6F; i++) {
		SendData(i, 0x00);
	}
}

void MAX6954::SendData(uint8_t address, uint8_t data) {
uint8_t tx[2] = {address, data};
	NSS_LOW();
	HAL_SPI_Transmit(&hspi, tx, 2, 100);
	NSS_HIGH();
}

uint8_t MAX6954::ReadReg(uint8_t address) {
uint8_t tx[4];

	tx[0] = address | 0x80;
	tx[1] = 0x00;
	tx[2] = 0x00;
	tx[3] = 0x00;

	NSS_LOW();
	HAL_SPI_TransmitReceive(&hspi, tx, tx, 2, 100);
	NSS_HIGH();

	tx[0] = address;
	tx[1] = 0x00;
	NSS_LOW();
	HAL_SPI_TransmitReceive(&hspi, tx, tx, 2, 100);
	NSS_HIGH();

	return tx[1];
}

void MAX6954::PrintCustom(LINE line, uint8_t seg1, uint8_t seg2, uint8_t seg3, uint8_t seg4) {
	if (xSemaphoreTake(xMutex, 10) == pdFALSE)
		return;

	SendData(lines[line][0], seg1);
	SendData(lines[line][1], seg2);
	SendData(lines[line][2], seg3);
	SendData(lines[line][3], seg4);

	xSemaphoreGive(xMutex);
}

void MAX6954::PrintCustom(LINE line, uint32_t segments) {
	if (xSemaphoreTake(xMutex, 10) == pdFALSE)
		return;

	SendData(lines[line][0], (uint8_t)((segments & 0xFF000000) >> 24));
	SendData(lines[line][1], (uint8_t)((segments & 0x00FF0000) >> 16));
	SendData(lines[line][2], (uint8_t)((segments & 0x0000FF00) >>  8));
	SendData(lines[line][3], (uint8_t)((segments & 0x000000FF) >>  0))
	;
	xSemaphoreGive(xMutex);
}

uint8_t MAX6954::DecodeInt(uint8_t data, uint8_t dp) {
	if (dp)
		return letters[data] | 0x80;

	return letters[data];
}

void MAX6954::PrintLine(LINE line, int32_t value) {

	if (xSemaphoreTake(xMutex, 10) == pdFALSE)
		return;

int32_t data = value;

	if (data < -999) {
		for (uint8_t i = 0; i < 4; i++)
			SendData(lines[line][i], 0x08);

		xSemaphoreGive(xMutex);
		return;
	} else if (data == 0) {
		SendData(lines[line][0], 0x00);
		SendData(lines[line][1], 0x00);
		SendData(lines[line][2], 0x00);
		SendData(lines[line][3], letters[0]);

		xSemaphoreGive(xMutex);
		return;
	} else if (data > 9999) {
		for (uint8_t i = 0; i < 4; i++)
			SendData(lines[line][i], 0x40);

		xSemaphoreGive(xMutex);
		return;
	}

char buf[5];
size_t charcounts;
	charcounts = 0;
	sprintf(buf, "%d", (int)data);
	charcounts = strlen(buf);

	for (uint8_t i = 0; i < 4; i++) {
		if (i < 4 - charcounts)
			SendData(lines[line][i], 0x00);
		else {
			if (buf[i + charcounts - 4] == '-')
				SendData(lines[line][i], 0x01);
			else
				SendData(lines[line][i], DecodeInt(buf[i + charcounts - 4] - '0', 0));
		}
	}

	xSemaphoreGive(xMutex);
}

void MAX6954::PrintLine(LINE line, float value) {
(void) line;
(void) value;

	if (xSemaphoreTake(xMutex, 10) == pdFALSE)
		return;

	if (value < -999.5f) {
		for (uint8_t i = 0; i < 4; i++)
			SendData(lines[line][i], 0x08);

		xSemaphoreGive(xMutex);
		return;
	} else if (value > 9999.4f) {
		for (uint8_t i = 0; i < 4; i++)
			SendData(lines[line][i], 0x40);

		xSemaphoreGive(xMutex);
		return;
	}

char buf[6];
int intvalue = (int)value;
size_t numint_characters, numfloat_characters;
	sprintf(buf, "%d", intvalue);
	numint_characters = strlen(buf);

	if (intvalue < 0)
		numint_characters--;

	if (value < 0.0f)
		floatToString(buf, value, 4 - numint_characters - 1);
	else
		floatToString(buf, value, 4 - numint_characters);

	numfloat_characters = strlen(buf);

bool dot = false; /* Если попадается точка, то переменная становится 1 */
	for (uint8_t i = 0; i < numfloat_characters; i++) {
		if (buf[i] == '.') {
			SendData(lines[line][i - 1], DecodeInt(buf[i - 1] - '0', 1));
			dot = true;
		} else {
			if (dot) {
				if (buf[i] == '-')
					SendData(lines[line][i - 1], 0x01);
				else
					SendData(lines[line][i - 1], DecodeInt(buf[i] - '0', 0));
			} else {
				if (buf[i] == '-')
					SendData(lines[line][i], 0x01);
				else
					SendData(lines[line][i], DecodeInt(buf[i] - '0', 0));
			}
		}
	}

	xSemaphoreGive(xMutex);
}

void MAX6954::PrintLine(LINE line, float value, uint8_t precission) {
(void) line;
(void) value;
(void) precission;

	if (xSemaphoreTake(xMutex, 10) == pdFALSE)
		return;

	for (uint8_t i = 0; i < 4; i++)
		SendData(lines[line][i], 0x01);

	xSemaphoreGive(xMutex);
}

char *MAX6954::floatToString(char *outstr, double value, int places, int minwidth, bool rightjustify) {
// this is used to write a float value to string, outstr.  oustr is also the return value.
int digit;
double tens = 0.1f;
int tenscount = 0;
double tempfloat = value;
int c = 0;
int charcount = 1;
int extra = 0;
	// make sure we round properly. this could use pow from <math.h>, but doesn't seem worth the import
	// if this rounding step isn't here, the value  54.321 prints as 54.3209

	// calculate rounding term d:   0.5/pow(10,places)
	double d = 0.5;
	if (value < 0)
		d *= -1.0;
	// divide by ten for each decimal place
	for (int i = 0; i < places; i++)
		d/= 10.0;
	// this small addition, combined with truncation will round our values properly
	tempfloat +=  d;

	// first get value tens to be the large power of ten less than value
	if (value < 0)
		tempfloat *= -1.0;
	while ((tens * 10.0) <= tempfloat) {
		tens *= 10.0;
		tenscount += 1;
	}

	if (tenscount > 0)
		charcount += tenscount;
	else
		charcount += 1;

	if (value < 0)
		charcount += 1;
	charcount += 1 + places;

	minwidth += 1; // both count the null final character
	if (minwidth > charcount){
		extra = minwidth - charcount;
		charcount = minwidth;
	}

	if ((extra > 0) && rightjustify) {
		for (int i = 0; i< extra; i++) {
			outstr[c++] = ' ';
		}
	}

	// write out the negative if needed
	if (value < 0)
		outstr[c++] = '-';

	if (tenscount == 0)
		outstr[c++] = '0';

	for (int i = 0; i < tenscount; i++) {
		digit = (int) (tempfloat / tens);
		sprintf(&outstr[c++], "%d", digit);
		tempfloat = tempfloat - ((float)digit * tens);
		tens /= 10.0;
	}

	// if no places after decimal, stop now and return

	// otherwise, write the point and continue on
	if (places > 0)
		outstr[c++] = '.';


	// now write out each decimal place by shifting digits one by one into the ones place and writing the truncated value
	for (int i = 0; i < places; i++) {
		tempfloat *= 10.0;
		digit = (int) tempfloat;
		sprintf(&outstr[c++], "%d", digit);

		// once written, subtract off that digit
		tempfloat = tempfloat - (float) digit;
	}
	if (extra > 0 && (!rightjustify)) {
		for (int i = 0; i < extra; i++) {
			outstr[c++] = ' ';
		}
	}

	outstr[c++] = '\0';
	return outstr;
}

void MAX6954::task() {
	if (xSemaphoreTake(xMutex, 1000) == pdTRUE) {
		SendData(0x04, 0x01 | (0 << 3)); // Включить тактирование
		SendData(0x03, 0x07);
		SendData(0x0C, 0x00);

		SendData(0x06, 0x20); /* 8 кнопок, P1, P2, P3 - выходы  */
		SendData(0x08, 0x3F); /* key mask register  */

		DisableDecodeMode();
		SetGlobalBrightness(BRIGHTNESS_7_16);
		ClearAll();
		ReadReg(0x88);
		xSemaphoreGive(xMutex);
	}

ButtonEvent event;
bool need_tx = false;
uint8_t btn;

	vTaskPrioritySet(handle, tskIDLE_PRIORITY);
	for (;;) {

		if (xSemaphoreTake(xSemaphoreISR, portMAX_DELAY) == pdTRUE) {
			if (xSemaphoreTake(xMutex, 100) == pdTRUE) {
				btn = ReadReg(0x8C);
				ReadReg(0x88);
				xSemaphoreGive(xMutex);
			} else {
				btn = 0xFF;
			}

			switch (btn) {
			case 0x01:
				event.Button = 1;
				need_tx = true;
				break;
			case 0x02:
				event.Button = 2;
				need_tx = true;
				break;
			case 0x04:
				event.Button = 3;
				need_tx = true;
				break;
			case 0x08:
				event.Button = 4;
				need_tx = true;
				break;
			case 0x10:
				event.Button = 5;
				need_tx = true;
				break;
			case 0x20:
				event.Button = 6;
				need_tx = true;
				break;
			default:
				need_tx = false;
				break;
			}

			if (need_tx == true) {
				event.State = BTN_PRESSED;
				event.Tick = xTaskGetTickCount();
				xQueueOverwrite(xQueueButtonEvent, (void *)&event);
				need_tx = false;
			}
		}
	}
}
