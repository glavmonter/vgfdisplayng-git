/*
 * canlib.cpp
 *
 *  Created on: 12 oct 2014.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 *
 *  Класс для обслуживания CAN и передачи сообщений на комп
 */


#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>
#include <new>
#include "tlsf.h"
#include "canlib.h"
#include "SEGGER_RTT.h"
#include "canmsg.h"

#include "pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "controller.pb.h"

static CANLib *canlib = NULL;

static void CAN_Receive(CAN_TypeDef *CANx, uint32_t FIFONumber, CanRxMsgTypeDef *RxMessage);
static uint32_t can_crc32(const unsigned char *buf, size_t len);

void CANLibLink(CANLib *ll) {
	canlib = ll;
}

void CANLibInit() {
	canlib = new CANLib("CANLib", tskIDLE_PRIORITY, 1, configMINIMAL_STACK_SIZE*3);
}

CANLib::CANLib(char const *name, unsigned portBASE_TYPE priority, uint8_t canID, uint16_t stackDepth) {
	DeviceID = canID;
	m_pPids = NULL;

	__CAN1_CLK_ENABLE();

	hcan.Instance 	= CAN1;
	hcan.Init.TTCM 	= DISABLE;
	hcan.Init.ABOM 	= DISABLE;
	hcan.Init.AWUM 	= DISABLE;
	hcan.Init.NART 	= DISABLE;
	hcan.Init.RFLM 	= DISABLE;
	hcan.Init.TXFP 	= DISABLE;
	hcan.Init.Mode 	= CAN_MODE_NORMAL;
	hcan.Init.SJW 	= CAN_SJW_1TQ;
	hcan.Init.BS1 	= CAN_BS1_14TQ;
	hcan.Init.BS2 	= CAN_BS2_6TQ;
	hcan.Init.Prescaler = 4;		// 500 kbit/s
	hcan.pTxMsg = (CanTxMsgTypeDef *)pvPortMalloc(sizeof(CanTxMsgTypeDef));
	HAL_CAN_Init(&hcan);

CAN_FilterConfTypeDef sFilterConfig;
	sFilterConfig.FilterNumber = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDLIST;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_16BIT;
	sFilterConfig.FilterIdHigh = GENID(DeviceID, CANMSG_ACK) << 5;
	sFilterConfig.FilterIdLow = GENID(DeviceID, CANMSG_NACK) << 5;
	sFilterConfig.FilterMaskIdHigh = GENID(DeviceID, CANMSG_DTH) << 5;
	sFilterConfig.FilterMaskIdLow = GENID(DeviceID, CANMSG_HTD) << 5;
	sFilterConfig.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.BankNumber = 14;
	HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);

	sFilterConfig.FilterNumber = 1;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDLIST;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_16BIT;
	sFilterConfig.FilterIdHigh = GENID(DeviceID, CANMSG_DATA) << 5;
	sFilterConfig.FilterIdLow = GENID(0, CANMSG_SYNC) << 5;
	sFilterConfig.FilterMaskIdHigh = GENID(0, CANMSG_EMERGENCY) << 5;
	sFilterConfig.FilterMaskIdLow = GENID(0, CANMSG_HEARTBEAT) << 5;
	sFilterConfig.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.BankNumber = 14;
	HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);

	/* Разрешаем прерывание по приему */
	__HAL_CAN_ENABLE_IT(&hcan, CAN_IT_FMP0);

	HAL_NVIC_SetPriority((IRQn_Type)CAN1_RX0_IRQn, 0x0D, 0x00);
	HAL_NVIC_EnableIRQ((IRQn_Type)CAN1_RX0_IRQn);

	CANRxQueue = xQueueCreate(10, sizeof(CanRxMsgTypeDef));
	xTaskCreate(task_can, name, stackDepth, this, priority, &handle);

	xMutex = xSemaphoreCreateBinary();
	xSemaphoreGive(xMutex);
}


CANLib::~CANLib() {
	vTaskDelete(handle);
}

void CANLib::Ping() {
	hcan.pTxMsg->StdId = GENID(DeviceID, CANMSG_HEARTBEAT);
	hcan.pTxMsg->RTR = CAN_RTR_DATA;
	hcan.pTxMsg->DLC = 1;
	hcan.pTxMsg->IDE = CAN_ID_STD;
	hcan.pTxMsg->Data[0] = 0xAA;
	HAL_CAN_Transmit(&hcan, 1000);
}

void CANLib::task() {
//	SEGGER_RTT_printf(0, "CAN: 0x%X\n", handle);
	hcan.pTxMsg->StdId = GENID(DeviceID, CANMSG_HEARTBEAT);
	hcan.pTxMsg->RTR = CAN_RTR_DATA;
	hcan.pTxMsg->DLC = 1;
	hcan.pTxMsg->IDE = CAN_ID_STD;
	hcan.pTxMsg->Data[0] = 0xFF;
	HAL_CAN_Transmit(&hcan, 1000);

CanRxMsgTypeDef RxMessage;

	for (;;) {
		if (xQueueReceive(CANRxQueue, (void *)&RxMessage, portMAX_DELAY) == pdTRUE) {
//			SEGGER_RTT_printf(0, "CANLib: 0x%X\n", RxMessage.StdId);
			/* Есть сообщение */
			switch (GETMSG(RxMessage.StdId)) {
			case CANMSG_SYNC:	/* По синхронизации также получаем и температуру холодного конца */
				if (RxMessage.DLC == 0) {
					SendToHost(); /* Отправляем просто данные */
				} else if (RxMessage.DLC == 1) {
					SendConfigToHost();
				} else if (RxMessage.DLC == 4) {
					/* TODO Тут получаем температуру холодного конца */
					ProcessColdJunction();
				}
				break;

			case CANMSG_HTD:
			case CANMSG_DATA:
				ReceiveFromHost(&RxMessage);
				break;

			default:
				break;
			}
		}
	}
}

void CANLib::SetPids(PID **p) {
	m_pPids = p;
}

void CANLib::SendConfigToHost() {
VGF_EepSettings settings;
uint8_t 		*txBuffer = NULL;
pb_ostream_t ostream = pb_ostream_from_buffer(txBuffer, VGF_EepSettings_size);
	settings.Zones_count = 8;

	for (uint8_t z = 0; z < settings.Zones_count; z++) {
		settings.Zones[z].Kp = 0.0f;
		settings.Zones[z].Ki = 0.0f;
		settings.Zones[z].Kd = 0.0f;
		settings.Zones[z].Gain = 32;
		settings.Zones[z].PwmMax = 0;
		settings.Zones[z].Number = z;
	}

	memset(txBuffer, 0, VGF_EepSettings_size);
	if (!pb_encode(&ostream, VGF_EepSettings_fields, &settings)) {
		vPortFree(txBuffer);
		return;
	}

size_t bytes_to_send = ostream.bytes_written;
uint32_t crc = 0;
	crc = can_crc32(txBuffer, bytes_to_send);

CanTxMsgTypeDef *TxMessage = hcan.pTxMsg;
	TxMessage->StdId = GENID(DeviceID, CANMSG_DTH);
	TxMessage->ExtId = 0;
	TxMessage->IDE = CAN_ID_STD;
	TxMessage->RTR = CAN_RTR_DATA;
	TxMessage->DLC = 6;

	TxMessage->Data[0] = bytes_to_send & 0xFF;
	TxMessage->Data[1] = (bytes_to_send & 0xFF00) >> 8;
	memcpy(&TxMessage->Data[2], &crc, 4);
	HAL_CAN_Transmit(&hcan, 10);

uint8_t num_messages = bytes_to_send / 7;

	TxMessage->StdId = GENID(DeviceID, CANMSG_DATA);
	TxMessage->ExtId = 0;
	TxMessage->IDE = CAN_ID_STD;
	TxMessage->RTR = CAN_RTR_DATA;
	TxMessage->DLC = 8;
	for (uint8_t i = 0; i < num_messages; i++) {
		TxMessage->Data[0] = i;
		memcpy(&TxMessage->Data[1], &txBuffer[i * 7], 7);
		HAL_CAN_Transmit(&hcan, 10);
	}

	if (bytes_to_send % 7) {
		TxMessage->DLC = 1 + bytes_to_send % 7;
		TxMessage->Data[0] = bytes_to_send / 7;
		memcpy(&TxMessage->Data[1], &txBuffer[bytes_to_send - bytes_to_send % 7], bytes_to_send % 7);
		HAL_CAN_Transmit(&hcan, 10);
	}

	vPortFree(txBuffer);
}

void CANLib::ProcessColdJunction() {

}

int32_t CANLib::SendToHost() {
VGF_Protocol  *protocol = NULL;
uint8_t 	  *txBuffer	= NULL;

	if (xSemaphoreTake(xMutex, 200) != pdTRUE)
		return -2;

	txBuffer = (uint8_t *)pvPortMalloc(VGF_Protocol_size);
	protocol = (VGF_Protocol *)pvPortMalloc(sizeof(VGF_Protocol));

pb_ostream_t ostream = pb_ostream_from_buffer(txBuffer, VGF_Protocol_size);

	protocol->has_TCold = false;
	protocol->has_DeadZone_LL = false;
	protocol->has_DeadZone_UL = false;
	protocol->settings_count = 0;

	protocol->HtD_count = 0;
	protocol->DtH_count = 8;

	for (uint8_t z = 0; z < protocol->DtH_count; z++) {
		protocol->DtH[z].Number = z;
		protocol->DtH[z].CurrentT = 12.5f;
		protocol->DtH[z].PidError = 0.0f;
		protocol->DtH[z].Status = 0;
		protocol->DtH[z].PwmOut = 123;
	}

	float U = 0.0f, I = 0.0f;
	protocol->DtH[0].CurrentT = iadc->GetCPUTemperature();
	iadc->GetPowerUI(&U, &I);
	protocol->DtH[1].CurrentT = U;
	protocol->DtH[2].CurrentT = I;

	memset(txBuffer, 0, VGF_Protocol_size);
	if (!pb_encode(&ostream, VGF_Protocol_fields, protocol)) {
		vPortFree(txBuffer);
		vPortFree(protocol);

		xSemaphoreGive(xMutex);
		return -1;
	}

size_t bytes_to_send = ostream.bytes_written;
uint32_t crc = 0;
	crc = can_crc32(txBuffer, bytes_to_send);

CanTxMsgTypeDef *TxMessage = hcan.pTxMsg;
	TxMessage->StdId = GENID(DeviceID, CANMSG_DTH);
	TxMessage->ExtId = 0;
	TxMessage->IDE = CAN_ID_STD;
	TxMessage->RTR = CAN_RTR_DATA;
	TxMessage->DLC = 6;

	TxMessage->Data[0] = bytes_to_send & 0xFF;
	TxMessage->Data[1] = (bytes_to_send & 0xFF00) >> 8;
	memcpy(&TxMessage->Data[2], &crc, 4);

	HAL_CAN_Transmit(&hcan, 10);

uint8_t num_messages = bytes_to_send / 7; /* Количество полных сообщений */

	TxMessage->StdId = GENID(DeviceID, CANMSG_DATA);
	TxMessage->ExtId = 0;
	TxMessage->IDE = CAN_ID_STD;
	TxMessage->RTR = CAN_RTR_DATA;
	TxMessage->DLC = 8;

	for (uint8_t i = 0; i < num_messages; i++) {
		TxMessage->Data[0] = i;
		memcpy(&TxMessage->Data[1], &txBuffer[i * 7], 7);

		HAL_CAN_Transmit(&hcan, 10);
	}

	/* Передаем последние байты */
	if (bytes_to_send % 7) {
		TxMessage->DLC = 1 + bytes_to_send % 7;
		TxMessage->Data[0] = bytes_to_send / 7;

		memcpy(&TxMessage->Data[1], &txBuffer[bytes_to_send - bytes_to_send % 7], bytes_to_send % 7);
		HAL_CAN_Transmit(&hcan, 10);
	}

	xSemaphoreGive(xMutex);

	vPortFree(txBuffer);
	vPortFree(protocol);

CanRxMsgTypeDef RxMessage;
	if (xQueueReceive(CANRxQueue, (void *)&RxMessage, 100) == pdTRUE) {
		if (GETMSG(RxMessage.StdId) == CANMSG_ACK)
			return CAN_OK;
		else
			return CAN_ERROR_ACK;
	} else
		return CAN_ERROR_TIMEOUT;

	return CAN_OK;
}

int32_t CANLib::ReceiveFromHost(CanRxMsgTypeDef *RxMessage) {
static uint8_t *rxBuffer = NULL;
static size_t 	bytes_to_receive = 0;
static uint8_t 	num_msg = 0;
static uint8_t 	received_msg = 0;
static uint32_t crc;

uint32_t stack = uxTaskGetStackHighWaterMark(NULL);
//	SEGGER_RTT_printf(0, "Stack: %d\n", stack);


	if (GETMSG(RxMessage->StdId) == CANMSG_HTD) {
		/* Конфигурируем первое вхождение в прием */
		bytes_to_receive = RxMessage->Data[0];
		bytes_to_receive |= (RxMessage->Data[1] << 8);

		num_msg = bytes_to_receive / 7;
		if ((bytes_to_receive % 7) > 0)
			num_msg++;
		received_msg = num_msg;

		rxBuffer = (uint8_t *)pvPortMalloc(bytes_to_receive);
		if (rxBuffer == NULL) {
//			SEGGER_RTT_printf(0, "Allocate err: 0 NACK\n");
			SendNACK();
			return CAN_ERROR_MEMORY;
		}

		memcpy(&crc, &RxMessage->Data[2], 4);
//		SEGGER_RTT_printf(0, "HTD ok: 1\n");
		return CAN_OK;
	}

	/* Пришло сообщение с данными */
	if (GETMSG(RxMessage->StdId) == CANMSG_DATA) {
		if (rxBuffer == NULL) {
			SendNACK();
//			SEGGER_RTT_printf(0, "Allocate err: 2 NACK\n");
			return CAN_ERROR_MEMORY;
		}

		uint8_t index = RxMessage->Data[0];
		/* Индекс сообщения слишком большой */
		if (index >= num_msg) {
			vPortFree(rxBuffer);
			rxBuffer = NULL;
			SendNACK();
//			SEGGER_RTT_printf(0, "Index too big NACK\n");
			return CAN_ERROR_PB;
		}
		memcpy(&rxBuffer[index*7], &RxMessage->Data[1], RxMessage->DLC - 1);
		received_msg--;
	}

	if ((received_msg == 0) && (can_crc32(rxBuffer, bytes_to_receive) == crc)) {
		/* Приняли все сообщения */
		VGF_Protocol protocol;
		pb_istream_t istream = pb_istream_from_buffer(rxBuffer, bytes_to_receive);
//		SEGGER_RTT_printf(0, "All received, CRC Ok\n");

		if (!pb_decode(&istream, VGF_Protocol_fields, &protocol)) {
			vPortFree(rxBuffer);
			rxBuffer = NULL;
			SendNACK();
//			SEGGER_RTT_printf(0, "PB Error NACK\n");
			return CAN_ERROR_PB;
		} else {
			ProccessInputPb(&protocol);

//			SEGGER_RTT_printf(0, "Receive Ok, ACK\n");
			SendACK();
			vPortFree(rxBuffer);
			rxBuffer = NULL;
			return CAN_OK;
		}
	}

//	SEGGER_RTT_printf(0, "Unexpected error\n");
	return CAN_ERROR_UNEXP;
}

int32_t CANLib::ProccessInputPb(VGF_Protocol *pb) {

	return CAN_OK;
}


void CANLib::SendACK() {
CanTxMsgTypeDef *oldCanTx = hcan.pTxMsg;
CanTxMsgTypeDef newCanTx;

	newCanTx.StdId = GENID(DeviceID, CANMSG_ACK);
	newCanTx.ExtId = 0;
	newCanTx.IDE = CAN_ID_STD;
	newCanTx.RTR = CAN_RTR_DATA;
	newCanTx.DLC = 0;

	if (xSemaphoreTake(xMutex, 10) != pdTRUE)
		return;

	hcan.pTxMsg = &newCanTx;
	HAL_CAN_Transmit(&hcan, 10);
	hcan.pTxMsg = oldCanTx;

	xSemaphoreGive(xMutex);
}

void CANLib::SendNACK() {
CanTxMsgTypeDef *oldCanTx = hcan.pTxMsg;
CanTxMsgTypeDef newCanTx;

	newCanTx.StdId = GENID(DeviceID, CANMSG_NACK);
	newCanTx.ExtId = 0;
	newCanTx.IDE = CAN_ID_STD;
	newCanTx.RTR = CAN_RTR_DATA;
	newCanTx.DLC = 0;

	if (xSemaphoreTake(xMutex, 10) != pdTRUE)
		return;

	hcan.pTxMsg = &newCanTx;
	HAL_CAN_Transmit(&hcan, 10);
	hcan.pTxMsg = oldCanTx;

	xSemaphoreGive(xMutex);
}

/***********************************************************************************************************************************/
/* Вспомогательные функции */

/*
  Name  : CRC-32
  Poly  : 0x04C11DB7    x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11
                       + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
  Init  : 0xFFFFFFFF
  Revert: true
  XorOut: 0xFFFFFFFF
  Check : 0xCBF43926 ("123456789")
  MaxLen: 268 435 455 байт (2 147 483 647 бит) - обнаружение
   одинарных, двойных, пакетных и всех нечетных ошибок
*/
static const uint32_t Crc32Table[256] = {
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};

static uint32_t can_crc32(const unsigned char *buf, size_t len) {
	uint32_t crc = 0xFFFFFFFF;
	while(len--)
		crc = (crc >> 8) ^ Crc32Table[(crc ^ *buf++) & 0xFF];
	return crc ^ 0xFFFFFFFF;
}

/*
 * TODO Заменить размеры в CanRxMsgTypeDef.Data на uint8_t
 */
static void CAN_Receive(CAN_TypeDef *CANx, uint32_t FIFONumber, CanRxMsgTypeDef *RxMessage) {
	/* Get ID */
	RxMessage->IDE = (uint8_t)0x04 & CANx->sFIFOMailBox[FIFONumber].RIR;
	if (RxMessage->IDE == CAN_ID_STD) {
		RxMessage->StdId = (uint32_t)0x000007FF & (CANx->sFIFOMailBox[FIFONumber].RIR >> 21);
	} else {
		RxMessage->ExtId = (uint32_t)0x1FFFFFFF & (CANx->sFIFOMailBox[FIFONumber].RIR >> 3);
	}

	RxMessage->RTR = (uint8_t)0x02 & CANx->sFIFOMailBox[FIFONumber].RIR;

	/* Get DLC */
	RxMessage->DLC = (uint8_t)0x0F & CANx->sFIFOMailBox[FIFONumber].RDTR;

	/* Get FMI */
	RxMessage->FMI = (uint8_t)0xFF & (CANx->sFIFOMailBox[FIFONumber].RDTR >> 8);

	/* Get Data field */
	RxMessage->Data[0] = (uint8_t)(0xFF & CANx->sFIFOMailBox[FIFONumber].RDLR);
	RxMessage->Data[1] = (uint8_t)(0xFF & (CANx->sFIFOMailBox[FIFONumber].RDLR >> 8));
	RxMessage->Data[2] = (uint8_t)(0xFF & (CANx->sFIFOMailBox[FIFONumber].RDLR >> 16));
	RxMessage->Data[3] = (uint8_t)(0xFF & (CANx->sFIFOMailBox[FIFONumber].RDLR >> 24));
	RxMessage->Data[4] = (uint8_t)(0xFF & CANx->sFIFOMailBox[FIFONumber].RDHR);
	RxMessage->Data[5] = (uint8_t)(0xFF & (CANx->sFIFOMailBox[FIFONumber].RDHR >> 8));
	RxMessage->Data[6] = (uint8_t)(0xFF & (CANx->sFIFOMailBox[FIFONumber].RDHR >> 16));
	RxMessage->Data[7] = (uint8_t)(0xFF & (CANx->sFIFOMailBox[FIFONumber].RDHR >> 24));

	/* Release the FIFO0 */
	if (FIFONumber == CAN_FIFO0) {
		CANx->RF0R |= CAN_RF0R_RFOM0;
	} else {
		CANx->RF1R |= CAN_RF1R_RFOM1;
	}
}

void CAN1_RX0_IRQHandler(void) {
CanRxMsgTypeDef RxMessage;
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);

	xQueueSendFromISR(canlib->CANRxQueue, (void *)&RxMessage, &xHigherPriorityTaskWoken);
//		SEGGER_RTT_printf(0, "Send failed\n");

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
