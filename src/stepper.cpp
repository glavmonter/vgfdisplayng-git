/*
 * stepper.cpp
 *
 *  Created on: 16 oct. 2014
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */


#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>

#include "SEGGER_RTT.h"
#include "stepper.h"


void StepperStart() {
	Stepper *stepper = new Stepper("Step", tskIDLE_PRIORITY, configMINIMAL_STACK_SIZE);
}

void GetProgramm(int id, StepConfig *s) {
	s->period = sconf[id].period;
	s->prescaller = sconf[id].prescaller;
	s->speed = sconf[id].speed;
}


Stepper::Stepper(const char *name, unsigned portBASE_TYPE priority, uint16_t stackDepth): isStoped(true) {
	/* Выходы Enable (PD15), Direction (PD14) */

	__GPIOD_CLK_ENABLE();
GPIO_InitTypeDef GPIOIniStructure;

	/* Direction - PD14 */
	GPIOIniStructure.Pin = GPIO_PIN_14;
	GPIOIniStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIOIniStructure.Pull = GPIO_PULLDOWN;
	GPIOIniStructure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOD, &GPIOIniStructure);

	/* Enable - PD15 */
	GPIOIniStructure.Pin = GPIO_PIN_15;
	HAL_GPIO_Init(GPIOD, &GPIOIniStructure);
	Enable();

	htim8.Instance = TIM8;
	htim8.Init.Prescaler = SystemCoreClock/10000 - 1; 	// 10 kHz - Скорость счета таймера
	htim8.Init.Period = 10 - 1;							// Период выходной частоты  Prescaler/
	htim8.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim8.Init.CounterMode = TIM_COUNTERMODE_UP;
	HAL_TIM_OC_Init(&htim8);

	hoc1.OCMode = TIM_OCMODE_PWM1;
	hoc1.OCPolarity = TIM_OCPOLARITY_HIGH;
	hoc1.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	hoc1.OCIdleState = TIM_OCIDLESTATE_SET;
	hoc1.OCNIdleState = TIM_OCNIDLESTATE_SET;
	hoc1.OCFastMode = TIM_OCFAST_DISABLE;
	hoc1.Pulse = 0;
	HAL_TIM_PWM_ConfigChannel(&htim8, &hoc1, TIM_CHANNEL_1);

	lTimer = xTimerCreate("Timer", 10, pdFALSE, (void *)this, TimerCall);

	xSemaphoreTimeOut = xSemaphoreCreateBinary();
	xSemaphoreTake(xSemaphoreTimeOut, 0);

	xQueueAddStep = xQueueCreate(3, sizeof(StepperProgram_t));

	xQueueSet = xQueueCreateSet(1 + 3);
	xQueueAddToSet(xSemaphoreTimeOut, xQueueSet);
	xQueueAddToSet(xQueueAddStep, xQueueSet);

	xTaskCreate(task_stepper, name, stackDepth, this, priority, &handle);
}


Stepper::~Stepper() {
	vTaskDelete(handle);
}

void Stepper::AddProgramStep(double Speed_mm_h, uint32_t Time, bool stop) {
StepperProgram_t prg;
	prg.Speed_mm_h = Speed_mm_h;
	prg.Time = Time;
	if (stop == true)
		prg.Time = 0;
	xQueueSend(this->xQueueAddStep, (void *)&prg, 10);
}


void Stepper::task() {
//	SEGGER_RTT_printf(0, "Stepper: 0x%X\n", handle);

QueueSetMemberHandle_t activate;
	xSemaphoreGive(xSemaphoreTimeOut);

	for (;;) {
		activate = xQueueSelectFromSet(xQueueSet, portMAX_DELAY);

		if (activate == xSemaphoreTimeOut) {
			HAL_TIM_OC_Stop(&htim8, TIM_CHANNEL_1);
			xSemaphoreTake(xSemaphoreTimeOut, 0);

			/* Тут у нас прошел таймаут прошлого задания */
			if (listProgram.size() > 0) {
				StepperProgram_t prg = listProgram.back();
				listProgram.pop_back();

				double Frequency = 2857.0*prg.Speed_mm_h*200.0/(3600.0*(1.0/2));
				htim8.Init.Period = (uint32_t)(10000.0/Frequency - 1.0);
				HAL_TIM_OC_Init(&htim8);
				hoc1.Pulse = (htim8.Init.Period + 1) / 2;
				HAL_TIM_PWM_ConfigChannel(&htim8, &hoc1, TIM_CHANNEL_1);

				xTimerChangePeriod(lTimer, prg.Time, 0);
				xTimerStart(lTimer, 0);
				HAL_TIM_OC_Start(&htim8, TIM_CHANNEL_1);

				isStoped = false;
			} else {
				isStoped = true;
			}
		} else if (activate == xQueueAddStep) {
			/* Тут мы добавляем новые шаги в список*/
			StepperProgram_t prg;
			xQueueReceive(xQueueAddStep, &prg, 0);

			if (prg.Time == 0) {
				/* Остановить выполнение всех программ */
				listProgram.clear();
				xTimerStop(lTimer, 0);
				xSemaphoreGive(xSemaphoreTimeOut);
			} else {
				listProgram.push_front(prg);
			}

			/* Инициируем таймаут и запускаем задачу, если были до этого остановлены */
			if (isStoped == true) {
				isStoped = false;
				xSemaphoreGive(xSemaphoreTimeOut);
			}
		}
	}
}

