/*
 * pid.cpp
 *
 *  Created on: 06 oct 2014.
 *      Author: Vladimir Meshkov <glavmonter@gmail.com>
 */


#include <FreeRTOS.h>
#include <new>
#include "tlsf.h"
#include "common.h"
#include "pid.h"
#include "stm32f4xx_hal.h"
#include "SEGGER_RTT.h"


PID::PID(char const *name, unsigned portBASE_TYPE priority, uint8_t number,
			uint16_t stackDepth):lastTick(0) {

	m_uNumber = number;
	m_xQueueADCIn = xQueueCreate(10, sizeof(float));
	xTaskCreate(task_pid, name, stackDepth, this, priority, &handle);
}

PID::~PID() {
	vTaskDelete(this->handle);
}


void PID::task() {

	for (;;) {
		float uV;
		if (xQueueReceive(m_xQueueADCIn, (void *)&uV, portMAX_DELAY) == pdTRUE) {
			TickType_t currentTick = xTaskGetTickCount();
//			SEGGER_RTT_printf(0, "PID[%d]: %d\n", m_uNumber, currentTick - lastTick);
			lastTick = currentTick;
		}
	}
}

BaseType_t PID::PidADCUpdate(float uV) {
	if (m_xQueueADCIn != NULL)
		return xQueueSend(m_xQueueADCIn, (void *)&uV, 10);
	else
		return errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY;
}

void PID::PrintHandle() {
//	SEGGER_RTT_printf(0, "PID handle: 0x%X\n", handle);
}
